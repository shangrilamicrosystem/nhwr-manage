<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
// DB_NAME
// DB_USER
// DB_PASS
// DB_HOST
// DB_PORT 
require "logger-config.php";

function connect() {

	$conn = new mysqli( DB_HOST, DB_USER, DB_PASS, DB_NAME, DB_PORT );
	
	if ( mysqli_connect_errno() ) {
		printf("Connection failed: %s", mysqli_connect_error());
		exit();
	}
	return $conn;
	
}

function dump_person_record($record){
    $conn = connect(); 
    
    $source = json_encode($record);
    
    $uuid = $record['uuid'];
    $firstname = $record['firstname'];
    $middle_name = $record['middle_name'];
    $surname = $record['surname'];
    $maiden_name = $record['maiden_name'];
    $othername = $record['othername'];
    
    $citizenship = $record['citizenship'];
    $citizenship_secondary = $record['citizenship_secondary'];
    $residence_country = $record['residence_country'];
    $residence_region = $record['residence_region'];
    $residence_district = $record['residence_district'];
    $residence_vdc_mun = $record['residence_vdc_mun'];
    $ward_number = $record['ward_number'];
    $street_name = $record['street_name'];
    $house_number = $record['house_number'];
    $nationality = $record['nationality'];
    $residence = $record['residence'];
    $nationality_secondary = $record['nationality_secondary'];
    $temp_address = $record['temp_address'];
    
    $sql = "INSERT INTO records (`uuid`, `firstname`, `middle_name`, `surname`, `citizenship`, `citizenship_secondary`, `residence_country`, `residence_region`, `residence_district`, `residence_vdc_mun`, `ward_number`, `street_name`, `house_number`, `nationality`, `residence`, `nationality_secondary`, `temp_address`, `source` ) values('$uuid', '$firstname', '$middle_name', '$surname','$citizenship', '$citizenship_secondary','$residence_country', '$residence_region', '$residence_district','$residence_vdc_mun', '$ward_number', '$street_name','$house_number', '$nationality','$residence','$nationality_secondary', '$temp_address', '$source')";
    
    if ($conn->query($sql) === TRUE) {
        echo "New record created successfully \n";
        return $conn->insert_id;
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
        return null;
    }
    $conn->close();
}

function writeLog($r_id, $message=''){
    $conn = connect(); 
    $sql = "INSERT INTO logger (`record_id`,`message`) VALUES ('$r_id', '$message')";
    
    if ($conn->query($sql) === TRUE) {
        echo "New record created successfully \n";
        return $conn->insert_id;
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
        return null;
    }
    $conn->close();
}


function update_file_status($id, $status){
    $conn = connect();
    
    $sql = "Update import_file_upload set status ='". $status. "' WHERE id=".$id;
    
    if ($conn->query($sql) === TRUE) {
        echo "Record Status Updated to $status \n";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
    $conn->close();
}

function get_file_path($id){
    $sql = "select * from import_file_upload where id=".$id;
    $conn = connect();
    $result = $conn->query($sql);
    $row = $result->fetch_assoc();
    if( !$row ) return false;
    if($row['status'] == 'Pending'){
        return $row;
    }
    return false;
}