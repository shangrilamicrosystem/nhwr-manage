#!/usr/bin/php
<?php
/*
 * © Copyright 2007, 2008 IntraHealth International, Inc.
 *
 * This File is part of iHRIS
 *
 * iHRIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * The page wrangler
 *
 * This page loads the main HTML template for the home page of the site.
 * @package iHRIS
 * @subpackage DemoManage
 * @access public
 * @author Sovello Hildebrand sovellohpmgani@gmail.com
 * @copyright Copyright &copy; 2007, 2008-2013 IntraHealth International, Inc.
 * @version 4.6.0
 */
/*
php import_data.php ./data/person/person_2_4.json
php /var/lib/iHRIS/sites/nhwr-ihris/pages/import_data.php /var/lib/iHRIS/sites/nhwr-ihris/pages/data/person/person_2_4.json
php import_data.php /path/to/your/excel_sheet.csv
*/
require_once("./import_base.php");
require_once("logger.php");

class PersonalData_Import extends Processor{
    public $person_log_record_id;
    public $current_file_processing_id;
	public function __construct($file, $ask, $id=null) {
	        $this->current_file_processing_id = $id;
	    	parent::__construct($file, $ask);
			
	}

	//map headers from the spreadsheet
	//what you do here is change the values on the right to match what you have on the spreadsheet. comment out lines that are not in the spreadsheet
	//the values of the left are used by the script to refer to the spreadsheet columns on the right of this array.
	//the order of the columns in the spreadsheet doesn't matter

	protected function getExpectedHeaders(){
		return array(
			'uuid'		=> 'uuid',
			"firstname" => "firstname",
			"middle_name" => "middle_name",
			"surname" => "surname",
			
// 			"maiden_name" => "maiden_name",
// 			"othername" => "othername",
// 			"othername_2" => "othername_2",
// 			"othername_3" => "othername_3",
// 			"citizenship" => "citizenship",
// 			"citizenship_secondary" => "citizenship_secondary",
			
// 			"ward_number" => "ward_number",
// 			"street_name" => "street_name",
// 			"house_number" => "house_number",
			
			"nationality" => "nationality",
			"residence" => "residence",
// 			"nationality_secondary" => "nationality_secondary",

			"temp_address" => array("field_name" =>"temp_address", "is_mendatory" => false),
			"first_name_np" => "first_name_np",
			"middle_name_np" => "middle_name_np",
			"last_name_np" => "last_name_np",
			
		
			"person_id"	=> "person_id",
			"demographic" => "demographic",
			"person_contact_personal" => "person_contact_personal",
// 			"person_contact_emergency" => "person_contact_emergency",
			"person_contact_emergency" => array(
				"field_name" => "person_contact_emergency",
				"is_mandatory" => false),
			"person_parent_info" => "person_parent_info",
			
			'person_position'       => 'person_position',
			'education'             => 'education',
			'registration'          => 'registration'

		);
	}

	/**
	 * this function returns the id numbers 'id_number' and id type 'id_type' for that id number.
	 */

	public function getIdNumberArray(){
		//when in the spreadsheet you have multiple columns that refer to identification numbers then you have this array to handle just that
		//with the columns set above, here you come map the columns to their specific id_type.
		//so if you have say just one column with identification numbers then comment lines 80 to 87 of this file by typing /* on line 79
		//and */ on line 89
		//change the the value in id_type to be e.g. National ID to match the type of identification for this column
		return $id_numbers = array(
			0 => array(
					'id_number' => $this->mapped_data['id_num'],
					'id_type' => 'Payroll Number'
				),
			/*
			1 => array(
					'id_number' => $this->mapped_data['id_num1'],
					'id_type' => 'Salary Number'
				),
			2 => array(
				 'id_number' => $this->mapped_data['id_num'],
					'id_type' => 'Payroll Number'
			*/

		);
	}

	//this part checks to see if the nationality/country column for a record is empty it defaults to Tanzania.
	//change this to your country. otherwise it takes the value of the country in that column
	public function getNationality(){
		return empty($this->mapped_data['nationality']) ? 'Nepal' : $this->mapped_data['nationality'];
	}

	//in this part comment out if you are not adding any data for that specific item.
	//for example if there is no nextofkin data in the spreadsheet,
	//comment out line 108 by preceding it with double-slasses as in this line
	//remember to also comment out all lines in the getExpectedHeaders() function lines 46-63
	protected function _processRow(){
	    echo "write recored";
	    I2CE::raiseMessage("Write Record to database \n");
	    $this->person_log_record_id = dump_person_record($this->mapped_data);
	   // exit("give me latet id");
	    I2CE::raiseMessage("Record id from database". $this->person_log_record_id);
	    writeLog($this->person_log_record_id, "New Record Inserted");
	    writeLog($this->person_log_record_id, "Processing");
	    
	    if($this->current_file_processing_id){
	        update_file_status($this->current_file_processing_id, "Importing");
	    }
	    
	    
	    
		$personId = $this->findPerson($this->mapped_data);
		
		
		I2CE::raiseMessage("personId = $personId");
		if(strlen($personId)==0 || $personId == -1)
			return;
		
		writeLog($this->person_log_record_id, "Record ID ". $personId);
		$this->updatePersonPosition($personId, $this->mapped_data["person_position"]);
		$this->updatePersonEducation($personId, $this->mapped_data["education"]);
		
		//start updating the child data
		$this->updatePersonIds($personId, $this->mapped_data['person_id']);
		$this->updateDemographic($personId, $this->mapped_data['demographic']);
		$this->updateParentInfo($personId, $this->mapped_data['person_parent_info']);
		$this->updatePersonalContact($personId, $this->mapped_data["person_contact_personal"]);
		$this->updateEmergencyContact($personId, $this->mapped_data["person_contact_emergency"]);
		$this->updateRefrences($personId, $this->mapped_data["nepal_person_refrences"]);
		
        $this->updatePersonRegistration($personId, $this->mapped_data["registration"]);
        
        if($this->current_file_processing_id){
	        update_file_status($this->current_file_processing_id, "Imported");
	    }
	}

	protected function updatePersonIds($personId, $Ids = array() ){
		if( ($personObj = $this->ff->createContainer($personId)) instanceof iHRIS_Person ){
			foreach( $Ids as $index=>$data){
			    //find existing record by id_type and number and ignore this one
			    // if new record found then inserted
			    I2CE::raiseMessage("find persion by persion id". $data['id_num']. "// ". $data['id_type']);
			    
			    if(strlen(trim($data['id_num'])) == 0 || strlen(trim($data['id_type'])) == 0 )  continue;
			    if( empty(trim($data['id_num'])) || empty(trim($data['id_type']))) continue;
			    if( empty(trim($data['id_num'])) || empty(trim($data['issue_date'])) || empty(trim($data['place'])) ) continue;
			    
			    $found_ids = $this->findPersonIDExitsOrNot($data['id_num'], $data['id_type'], $data['issue_date'], $data['place'], $personId);
			    
			    if(!$found_ids){
			        $formObj = $this->ff->createContainer('person_id');
    				
			    }else{
			        $formObj = $this->ff->createContainer('person_id|'.$found_ids['id']);
			    }
			    $formObj->populate();
			    
			    if(!empty($data['id_num']))
			        $formObj->getField('id_num')->setValue(trim($data['id_num']));
			    if(!empty($data['id_type']))
				    $formObj->getField('id_type')->setValue(array('id_type', $this->idTypeExists($data['id_type']) ) );
				if(!empty($data['country']))
				    $formObj->getField('country')->setValue(array('country', $this->countryExists($data['country']) ) );
				
				if(!empty($data['issue_date']))
				    $formObj->getField('issue_date')->setValue(I2CE_Date::fromDB(trim($data['issue_date'])));
				if(!empty($data['expiration_date']))
				    $formObj->getField('expiration_date')->setValue(I2CE_Date::fromDB(trim($data['expiration_date'])));
				if(!empty($data['place']))
				    $formObj->getField('place')->setValue(trim($data['place']));
				
				$formObj->setParent($personId);
				$this->save($formObj);
			    
			}
		}
	}
	
	protected function updatePersonTraining($personId, $Ids = array() ){
		if( ($personObj = $this->ff->createContainer($personId)) instanceof iHRIS_Person ){
			foreach( $Ids as $index=>$data){
				$formObj = $this->ff->createContainer('nepal_training');
				$formObj->getField('country')->setValue(array('country', $this->countryExists($data['country']) ) );
				$formObj->getField('institution')->setValue(trim($data['institution']));
				$formObj->getField('location_other')->setValue(trim($data['location_other']));
				$formObj->getField('started_date')->setValue(I2CE_Date::fromDB(trim($data['started_date'])));
				$formObj->getField('completed_date')->setValue(I2CE_Date::fromDB(trim($data['completed_date'])));
				$formObj->getField('minor')->setValue(trim($data['minor']));
				
				$formObj->setParent($personId);
				$this->save($formObj);
			}
		}
	}

	protected function updateParentInfo($personId, $parentDetail){
	    
		if(!$personId or count($parentDetail) == 0){
		    return;
		}
		
		$where = array(
			'operator' => 'FIELD_LIMIT',
			'field' => 'parent',
			'style' => 'lowerequals',
			'data' => array(
					'value' => $personId
				)
			);

		$parentId = I2CE_FormStorage::search('person_parent_info', false, $where );
		if( count($parentId) == 0){
			$formObj = $this->ff->createContainer('person_parent_info');
		}
		else
			$formObj = $this->ff->createContainer('person_parent_info|'.current($parentId));
		
		 $formObj->populate();
		 
		$fields = array('father_surname', 'father_firstname', 'father_middlename', 
		            'mother_surname', 'mother_firstname', 'mother_middlename', 
		            'spouse_firstname', 'spouse_middlename', 'spouse_surname', 
		            'grand_father_firstname', 'grand_father_middlename', 'grand_father_surname');
		foreach($fields as $field){
		    $value = trim($parentDetail[$field]);
		    
		    if( !empty($value) and strlen($value) > 0 ) {
		        $formObj->getField($field)->setValue(trim($parentDetail[$field]));
		    }else{
		        $formObj->getField($field)->setValue( $formObj->getField($field)->getDBValue() );
		    }
		}
		
		$formObj->setParent($personId);
		$this->save($formObj);
	}

	protected function updatePersonalContact($personId, $contact){
		if(strlen($personId)==0 or count($contact) == 0 )
			return;
		if(trim($contact['telephone']) == 0 && trim($contact['mobile_phone']) && trim($contact['email']))
			return;
			
		if( empty(trim( $contact['telephone']) ) or empty(trim($contact['email']))) return;
		
		$where = array(
			'operator' => 'FIELD_LIMIT',
			'field' => 'parent',
			'style' => 'lowerequals',
			'data' => array(
					'value' => $personId
				)
			);

		$parentId = I2CE_FormStorage::search('person_contact_personal', false, $where );
		if( count($parentId) == 0){
			$formObj = $this->ff->createContainer('person_contact_personal');
		}
		else
			$formObj = $this->ff->createContainer('person_contact_personal|'.current($parentId));
		
		$formObj->populate();
		
		$fields = array('telephone', 'mobile_phone', 'email');
		foreach($fields as $field){
		    $value = trim($contact[$field]);
		    
		    if( !empty($value) and strlen($value) > 0 ) {
		        $formObj->getField($field)->setValue(trim($contact[$field]));
		    }else{
		        $formObj->getField($field)->setValue( $formObj->getField($field)->getDBValue() );
		    }
		}
		
		$formObj->setParent($personId);
		$this->save($formObj);
	}

	protected function updateEmergencyContact($personId, $contact){
		if(!$personId or !is_array($contact) or count($contact) == 0 ){
		    return ;
		}
		
		$where = array(
			'operator' => 'FIELD_LIMIT',
			'field' => 'parent',
			'style' => 'lowerequals',
			'data' => array(
					'value' => $personId
				)
			);

		$parentId = I2CE_FormStorage::search('person_contact_emergency', false, $where );
		if( count($parentId) == 0){
			$formObj = $this->ff->createContainer('person_contact_emergency');
		}
		else
			$formObj = $this->ff->createContainer('person_contact_emergency|'.current($parentId));
			
		$formObj->populate();
		
		$fields = array('surname', 'middlename', 'firstname', 'telephone', 'mobile_phone', 'alt_telephone', 'email', 'mailing_address');
		foreach($fields as $field){
		    $value = trim($contact[$field]);
		    
		    if( !empty($value) and strlen($value) > 0 ) {
		        $formObj->getField($field)->setValue(trim($contact[$field]));
		    }else{
		        $formObj->getField($field)->setValue( $formObj->getField($field)->getDBValue() );
		    }
		}
		
		$formObj->setParent($personId);
		$this->save($formObj);
	}
	
	
	protected function updateRefrences($personId, $refrences ){
	    if(empty($personId) || !is_array($refrences) || count($refrences) == 0 ) return;
	    
	    foreach($refrences as $refrence){
	        $refId = $this->findPersonRefrence($personId, $refrence);
	        
	        if(!$refId){
    			$formObj = $this->ff->createContainer('nepal_person_refrences');
    		}
    		else{
    			$formObj = $this->ff->createContainer('nepal_person_refrences|'.($refId['id']));
    		}
    			
    		$formObj->populate();
    		
    		$fields = array('registration_number', 'full_name', 'location_other');
    		
    		foreach($fields as $field){
    		    $value = trim($refrence[$field]);
    		    if( !empty($value) and strlen($value) > 0 ) {
    		        $formObj->getField($field)->setValue(trim($refrence[$field]));
    		    }
    		}
    		
    		if(trim($refrence['date'])){
    		    $formObj->getField('date')->setValue( I2CE_Date::fromDB(trim($refrence['date'])) );
    		}
    		
    		$formObj->setParent($personId);
    		$this->save($formObj);
	    }
	}
	/****************************************************************************
	 *                                                                          *
	 *   DON'T EDIT BEYOND THIS POINT UNLESS YOU KNOW WHAT YOU WANT TO ACHIEVE  *
	 *                                                                          *
	 ****************************************************************************/
	protected function createPersonId($personId, $id_nums = array() ){
		if( ($personObj = $this->ff->createContainer($personId)) instanceof iHRIS_Person ){
			foreach( $id_nums as $index=>$data){
			    $formObj = $this->ff->createContainer('person_id');
				$formObj->getField('id_num')->setValue(trim($data['id_number']));
				$formObj->getField('id_type')->setValue(array('id_type', $this->idTypeExists($data['id_type']) ) );
				$formObj->setParent($personId);
				$this->save($formObj);
			}
		}
	}

    protected function updatePersonSource($personId, $source){
        if(strlen($personId)==0 or strlen($source) == 0 )
			return;
		
		$where = array(
			'operator'=>'AND',
			'operand'=>array(
				0=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'parent',
						'style'=>'lowerequals',
						'data'=>array( 'value'=>trim($personId) )
					),
				1=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'source_id',
						'style'=>'lowerequals',
						'data'=>array( 'value'=>trim($source) )
					)
				)
			);
			
		$sourceId = I2CE_FormStorage::search('nepal_person_source', false, $where );
		
		if( count($sourceId) == 0 ){
		    //insert into source history 
    		$formObj = $this->ff->createContainer('nepal_person_source');
    		$formObj->getField('source_id')->setValue(trim($source));
    		$formObj->setParent($personId);
    		$this->save($formObj);
		}
		
		return;
		
    }
    
	protected function updateDemographic($personId, $demo){
		if(strlen($personId)==0 or count($demo) == 0 )
			return;
		
		$where = array(
			'operator' => 'FIELD_LIMIT',
			'field' => 'parent',
			'style' => 'lowerequals',
			'data' => array(
					'value' => $personId
				)
			);

		$demoId = I2CE_FormStorage::search('demographic', false, $where );
		// echo current($demoId); exit;
		I2CE::raiseMessage("personId = $personId, demoId count ".count($demoId));
		if( count($demoId) == 0){
			$formObj = $this->ff->createContainer('demographic');
		}
		else
			$formObj = $this->ff->createContainer("demographic|".current($demoId));
        
        $formObj->populate();
		
		if(!empty(trim($demo['birth_date'])))
	    	$formObj->getField('birth_date')->setValue(I2CE_Date::fromDB(trim($demo['birth_date'])));
	    
	    if(!empty(trim($demo['gender'])))
	        $formObj->getField('gender')->setValue( array('gender', $this->getGender($demo['gender'])));
	    
	    if(!empty(trim($demo['marital_status'])))
		    $formObj->getField('marital_status')->setValue( array('marital_status', $this->maritalStatusExists($demo['marital_status'])) );
		if(!empty(trim($demo['dependents'])))
		    $formObj->getField('dependents')->setValue($demo['dependents']);
		
		
		if(!empty(trim($demo['citizenship_at_birth']))){
		    $formObj->getField('nationality_at_birth')->setValue( array('country', $this->countryExists($demo['citizenship_at_birth']) ));
		}
		    
		if(!empty(trim($demo['birth_location']['district']))){
		    $formObj->getField('birth_location')->setValue(array('district', $this->districtExists($demo['birth_location']['district']) ));
		    
		  //  $formObj->getField('birth_location_other')->setValue($demo['birth_location']['district']);
		}
		
		$formObj->setParent($personId);
		$this->save($formObj);
	}

	public function countryExists($name){
		return $this->checkNameExists('country', $name);
	}
	public function countyExits($name){
	    return $this->checkNameExists('county', $name);
	}
	public function countyExistsOrCreate($data){
	    if( !is_array($data)) return;
	    
	    if( empty(trim($data['county']))) return null;
	    
	    
	    $value_t = strtolower(trim($data['county']));
	    
        $where = array(
            'operator'=>'FIELD_LIMIT',
            'field'=>"name",
            'style'=>'lowerequals',
            'data'=>array( 'value' => $value_t )
        );
        if(strlen($value_t) > 0){
            $items = I2CE_FormStorage::search("county", false, $where);
        }else{
            return null;
        }
        if(count($items) >= 1 ){
            return current($items); //like 123432
        }
        elseif(count($items) == 0){
            
            $formObj = $this->ff->createContainer("county");
            $formObj->getField("name")->setValue($data['county']);
            $formObj->getField('district')->setValue(array('district', $this->districtExistsOrCreate($data) ) );
            
            $id = $this->save( $formObj );
            return $id; //like 123432
        }
        return null;
	}
	
	public function regionExists($name){
		return $this->checkNameExists('region', $name);
	}
	public function currencyExists($name){
		return $this->checkNameExists('currency', $name);
	}
	
	public function idTypeExists($name){
		return $this->checkNameExists('id_type', $name);
	}
	public function jobExists($title){
		return $this->checkTitleExists('job', $title);
	}
	public function facilityExists($name){
		return $this->checkNameExists('facility', $name);
	}
	public function facilityTypeExists($name){
	    return $this->checkNameExists('facility_type', $name);
	}
	public function ownershipTypeExists($name){
	    return $this->checkNameExists('ownership_type', $name);
	}
	public function facilityExistsOrCreate($data){
	    if( !is_array($data)) return;
	    
	    if( empty(trim($data['name']))) return null;
	    
	    $value_t = strtolower(trim($data['name']));
        $where = array(
            'operator'=>'FIELD_LIMIT',
            'field'=>"name",
            'style'=>'lowerequals',
            'data'=>array(
                    'value' => $value_t
                )
        );
        if(strlen($value_t) > 0){
            $items = I2CE_FormStorage::search("facility", false, $where);
        }else{
            return null;
        }
        if(count($items) >= 1 ){
            return current($items); //like 123432
        }
        elseif(count($items) == 0){
            
            $formObj = $this->ff->createContainer("facility");
            $formObj->getField("name")->setValue($data['name']);
            $formObj->getField('facility_type')->setValue(array('facility_type', $this->facilityTypeExists($data['facility_type']) ) );
            
            $formObj->getField('ownership_type')->setValue(array('ownership_type', $this->ownershipTypeExists($data['ownership_type']) ) );
            
            $formObj->getField('location')->setValue(array('district', $this->districtExists($data['location_district']) ) );
            
            
            $id = $this->save( $formObj );
            
            return $id; //like 123432
        }
        return null;
	}
	public function councilExists($name){
		return $this->checkNameExists('council', $name);
	}
	public function registrationCategoryExists($name){
	    return $this->checkNameExists('document_category', $name);
	}
	
	public function districtExistsOrCreate($data){
	    if( !is_array($data)) return;
	    
	    if( empty(trim($data['district']))) return null;
	    
	    $value_t = strtolower(trim($data['district']));
	    
        $where = array(
            'operator'=>'FIELD_LIMIT',
            'field'=>"name",
            'style'=>'lowerequals',
            'data'=>array( 'value' => $value_t )
        );
        
        if(strlen($value_t) > 0){
            $items = I2CE_FormStorage::search("district", false, $where);
        }else{
            return null;
        }
        if(count($items) >= 1 ){
            return current($items); //like 123432
        }
        elseif(count($items) == 0){
            
            $formObj = $this->ff->createContainer("district");
            $formObj->getField("name")->setValue($data['district']);
            $formObj->getField('region')->setValue(array('region', $this->regionExists($data['region']) ) );
            
            $id = $this->save( $formObj );
            return $id; //like 123432
        }
        return null;    
	}
	
	public function districtExists($name){
	    return $this->checkNameExists('district', $name);
	}
	
	public function institutionExists($name){
	    return $this->checkNameExists('institution', $name);
	}
	
	public function degreeExists($name){
	    return $this->checkNameExists("degree", $name);
	}
	public function degreeExistsOrCreate( $data ){
	    if( !is_array($data)) return;
	    
	    if( empty(trim($data['name']))) return null;
	    
	    $value_t = strtolower(trim($data['name']));
        $where = array(
            'operator'=>'FIELD_LIMIT',
            'field'=>"name",
            'style'=>'lowerequals',
            'data'=>array(
                    'value' => $value_t
                )
        );
        if(strlen($value_t) > 0){
            $items = I2CE_FormStorage::search("degree", false, $where);
        }else{
            return null;
        }
        if(count($items) >= 1 ){
            return current($items); //like 123432
        }
        elseif(count($items) == 0){
            
            $formObj = $this->ff->createContainer("degree");
            $formObj->getField("name")->setValue($data['name']);
            $formObj->getField('edu_type')->setValue(array('edu_type', $this->eduTypeExits($data['edu_type']) ) );
            
            $id = $this->save( $formObj );
            return $id; //like 123432
        }
        return null;
	}
    public function eduTypeExits($name){
        return $this->checkNameExists('edu_type', $name);
    }
	public function maritalStatusExists($name){
		return $this->checkNameExists('marital_status', $name);
	}

	public function getGender( $gender ){

		$g = strtolower(trim($gender));
		if( ($g == 'f') || ($g == 'female') || (substr($gender,0,1) == 'f') ){
			return 'F';
		}
		if( ($g == 'm') || ($g == 'male') || (substr($gender,0,1) == 'm') ){
			return 'M';
		}
	}

	public function findPersonByNames($surname, $firstname, $middle_name, $citizenship, $createRecord = true){
		//search person by names
		$wherePerson = array(
			'operator'=>'AND',
			'operand'=>array(
				0=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'surname',
						'style'=>'lowerequals',
						'data'=>array(
								'value'=>trim($surname)
								)
						),
				1=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'firstname',
						'style'=>'lowerequals',
						'data'=>array(
								'value'=>trim($firstname)
								)
						),
				2=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'middle_name',
						'style'=>'lowerequals',
						'data'=>array(
								'value'=>trim($middle_name)
								)
						)
				
				)
			);

		$person_ids = I2CE_FormStorage::search('person', false, $wherePerson);
		if( count($person_ids) > 1 || $createRecord == false){
			//$this->addBadRecord("This person already exists in duplicates");
			// return false;
			$arr = array();
			foreach ($person_ids as $key => $value) {
				$arr[] = 'person|'.$value;
			}
			return $arr;
			// return $person_ids;
		}
		elseif(count($person_ids) == 1){
			return 'person|'.$person_ids[0];
		}
		elseif( (count($person_ids) == 0) && createRecord ){
			//we need to create this person
			$personObj = $this->ff->createContainer('person');
			$personObj->firstname = trim($firstname);
			$personObj->othername = trim($othername);
			$personObj->surname = trim($surname);
			$personObj->nationality = array('country', $this->countryExists($country) );
			$personId = $this->save($personObj);
			return 'person|'.$personId;
		}
	}

	protected function createPerson($data, $person_id = false, $insert_source_record = false){
		$dt = $data;
		
		if( !$person_id ){
		    $personObj = $this->ff->createContainer('person');
			$personObj->uuid = trim($dt["uuid"]);
		}
		else{
		    echo "update existing =>". $person_id;
			$personObj = $this->ff->createContainer($person_id);
		}
    
    	$personObj->populate();
		 
		$fields = array('firstname', 'middle_name', 'surname', 
		            'maiden_name', 'othername', 'othername_2', 
		            'first_name_np', 'middle_name_np', 'last_name_np');
		            
		foreach($fields as $field){
		    $value = trim($dt[$field]);
		  
		    if( !empty($value) and strlen($value) > 0 ) {
		        $personObj->$field = trim($dt[$field]);
		    }
		}
		if( is_array($dt['temp_address'])){
    		if(!empty(trim($dt['temp_address']['county']))){
    		    $personObj->temp_address = array('county', trim($this->countyExistsOrCreate($dt['temp_address'])));
    		}
    		else if(!empty(trim($dt['temp_address']['district']))){
    		    $personObj->temp_address = array('district', trim($this->districtExistsOrCreate($dt['temp_address'])));
    		}
    		else if(!empty(trim($dt['temp_address']['region']))){
    		    $personObj->temp_address = array('region', $this->regionExists($dt['temp_address']['region']) );
    		}
    		else if(!empty(trim($dt['temp_address']['country']))){
    		    $personObj->temp_address = array('country', $this->countryExists($dt['temp_address']['country']) );
    		}
    		
    		if(!empty(trim($dt['temp_address']['house_number']))){
    		    $personObj->house_number = $dt['temp_address']['house_number'];
    		}
    		
    		if(!empty(trim($dt['temp_address']['house_number']))){
    		    $personObj->temp_address_house = $dt['temp_address']['house_number'];
    		}
    		
    		if(!empty(trim($dt['temp_address']['street_name']))){
    		    $personObj->temp_address_street = $dt['temp_address']['street_name'];
    		}
    		if(!empty(trim($dt['temp_address']['ward_number']))){
    		    $personObj->temp_address_ward = $dt['temp_address']['ward_number'];
    		}
    		
	    }
		// $personObj->firstname = trim($dt['firstname']);
		
		if(!empty(trim($dt['citizenship'])))
		    $personObj->citizenship = array('country', $this->countryExists($dt['citizenship']) );
		
		if(!empty(trim($dt['citizenship_secondary'])))
		    $personObj->citizenship_secondary = array('country', $this->countryExists($dt['citizenship_secondary']) );
		
        if(!empty(trim($dt['nationality'])))
		    $personObj->nationality = array('country', $this->countryExists($dt['nationality']) );
		
		if(!empty(trim($dt['nationality_secondary'])))
		    $personObj->nationality_secondary = array('country', $this->countryExists($dt['nationality_secondary']) );
		
		if( is_array($dt['residence'])){
    		if(!empty(trim($dt['residence']['county']))){
    		    $personObj->residence = array('county', trim($this->countyExistsOrCreate($dt['residence'])));
    		}
    		else if(!empty(trim($dt['residence']['district']))){
    		    $personObj->residence = array('district', trim($this->districtExistsOrCreate($dt['residence'])));
    		}
    		else if(!empty(trim($dt['residence']['region']))){
    		    $personObj->residence = array('region', $this->regionExists($dt['residence']['region']) );
    		}
    		else if(!empty(trim($dt['residence']['country']))){
    		    $personObj->residence = array('country', $this->countryExists($dt['residence']['country']) );
    		}
    		
    		if(!empty(trim($dt['residence']['house_number']))){
    		    $personObj->house_number = $dt['residence']['house_number'];
    		}
    		
    		if(!empty(trim($dt['residence']['street_name']))){
    		    $personObj->street_name = $dt['residence']['street_name'];
    		}
    		if(!empty(trim($dt['residence']['ward_number']))){
    		    $personObj->ward_number = $dt['residence']['ward_number'];
    		}
		}
		
		
		$personId = $this->save($personObj);
		
		if( $insert_source_record ) {
    		//insert into source history 
    		$formObj = $this->ff->createContainer('nepal_person_source');
    		$formObj->getField('source_id')->setValue(trim($dt['uuid']));
    		$formObj->setParent("person|".$personId);
    		$this->save($formObj);
    		
    		I2CE::raiseMessage("Inserted new source history >> ". $dt['uuid']);
    		I2CE::raiseMessage("Inserted new source history for ". $personId);
		}
		
		
		return 'person|'.$personId;
	}
	// find person by person unique id
	protected function findPersonByUid($uuid){
		//search person by names
		$wherePerson = array(
			'operator'=>'AND',
			'operand'=>array(
				0=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'uuid',
						'style'=>'lowerequals',
						'data'=>array(
								'value'=>trim($uuid)
								)
						)
			)
		);

		$person_ids = I2CE_FormStorage::search('person', false, $wherePerson);
		if( is_array($person_ids)){
			$arr = array();
			foreach ($person_ids as $key => $value) {
				$arr[] = 'person|'.$value;
			}
			return current($arr);
		}
		return false;
	}

    protected function findPersonIDExitsOrNot($id_number, $id_type, $issue_date, $place, $personId){
        if( trim($id_number) == 0 ) $id_number = null;
        if( empty(trim($id_number)) || empty(trim($issue_date)) || empty(trim($place)) ) return false;
        
        //search person by identification
		$whereIdentification = array(
			'operator'=>'AND',
			'operand'=>array(
					0=>array(
							'operator'=>'FIELD_LIMIT',
							'field'=>'id_num',
							'style'=>'equals',
							'data'=>array( 'value'=>trim($id_number) )
					),
					1=>array(
							'operator'=>'FIELD_LIMIT',
							'field'=>'id_type',
							'style'=>'equals',
							'data'=>array( 'value'=>trim( 'id_type|'.$this->idTypeExists( $id_type )) )
					),
					2=>array(
							'operator'=>'FIELD_LIMIT',
							'field'=>'issue_date',
							'style'=>'equals',
							'data'=>array( 'value'=>trim($date))
					),
					3=>array(
							'operator'=>'FIELD_LIMIT',
							'field'=>'place',
							'style'=>'equals',
							'data'=>array( 'value'=>trim($data['place']))
					),
					4=>array(
							'operator'=>'FIELD_LIMIT',
							'field'=>'parent',
							'style'=>'lowerequals',
    						'data'=>array( 'value'=>trim($personId) )
    						
					)
					
				)
			);
			
		$person_ids = I2CE_FormStorage::listFields('person_id', array('parent', 'id'), false, $whereIdentification);
		
		if( $person_ids and count($person_ids) > 0){
		    return current($person_ids);
		}
		return false;
    }
    protected function findPersonBySource($uuid){
        //nepal_person_source
        //search person by names
		$wherePerson = array(
			'operator'=>'AND',
			'operand'=>array(
				0=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'source_id',
						'style'=>'lowerequals',
						'data'=>array(
								'value'=>trim($uuid)
								)
						)
			)
		);
		
		$sources_ids = I2CE_FormStorage::listFields('nepal_person_source', array('parent'), false, $wherePerson);
		
		if( ( count($sources_ids) == 0 )  ){
			return false;
		}
		else{
			$arr = array();
			foreach ($sources_ids as $key => $value) {
				$arr[] = $value['parent'];
			}
			return current($arr);
		}
    }
	
	protected function findPersonRefrence($personId, $refrence){
	    if(!$personId or count($refrence) == 0 ){
		    return false;
		}
        //search person by names
		$where = array(
			'operator'=>'AND',
			'operand'=>array(
				0=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'parent',
						'style'=>'lowerequals',
						'data'=>array(
								'value'=>trim($personId)
								)
						),
				1=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'registration_number',
						'style'=>'lowerequals',
						'data'=>array(
								'value'=>trim($refrence['registration_number'])
								)
						),
				2=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'full_name',
						'style'=>'lowerequals',
						'data'=>array(
								'value'=>trim($refrence['full_name'])
								)
						),
			    3=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'location_other',
						'style'=>'lowerequals',
						'data'=>array(
								'value'=>trim($refrence['location_other'])
								)
						)
				
				)
			);
			$refId = I2CE_FormStorage::listFields('nepal_person_refrences', array('parent', 'id'), false, $where);
		
		if( $refId and count($refId) > 0){
		    return current($refId);
		}
		return false;
		
	}
	public function findPerson($data){
	        
		// search person by unique id
		// if exits update if not create
		if(trim($data['uuid']) != ''){
			I2CE::raiseMessage("find persion from source by uuid");
			writeLog($this->person_log_record_id, "find persion from source by uuid");
			// $person_ids = $this->findPersonByUid($data['uuid']);
			
			// find person by source id from source list
			// if user id found just update the recod
			// if not found find user by id type
	        $person_id = $this->findPersonBySource($data['uuid']);
	        if( $person_id ){
	            writeLog($this->person_log_record_id, "Record Found by uuid ". $person_id);
	            //update recod
	            return $this->createPerson($data, $person_id );
	        }
		}
        
        // print_r($data['person_id']); exit;
		// if uniquer id not exits search by id type "Citizenship"
		I2CE::raiseMessage("inside find persion by Citizenship");
		
		if(is_array($data['person_id']) and count($data['person_id']) > 0){
			I2CE::raiseMessage("find persion by user ids");
			writeLog($this->person_log_record_id, "find persion by Citizenship");
			$found_key = array_search('Citizenship', array_column($data['person_id'], 'id_type'));
			$searchFrom = array();
			if($found_key > -1)
			    $searchFrom[] = $data['person_id'][$found_key];
			
			$found_key = array_search('Passport', array_column($data['person_id'], 'id_type'));
			if($found_key > -1)
			    $searchFrom[] = $data['person_id'][$found_key];
			
			$found_key = array_search('Voter Card', array_column($data['person_id'], 'id_type'));
			if($found_key > -1)
			    $searchFrom[] = $data['person_id'][$found_key];
			
			foreach($searchFrom as $key => $pdata){
			    writeLog($this->person_log_record_id, "find persion by user ". $pdata['id_num']. "=> ". $pdata['id_type']);
		        
		        if( empty(trim($pdata['id_num'])) || empty(trim($pdata['id_type'])) ) continue;
		        
		        //$id_number, $id_type
				$name_ids = $this->findPersonByID($pdata);
				
				if($name_ids and count($name_ids) == 1){
				    I2CE::raiseMessage("found persion by ". $pdata['id_type']);
			        writeLog($this->person_log_record_id, "Found persion by ". $pdata['id_type']);
				    return $this->createPerson($data, current($name_ids), true); #third params for insert source
				}
		    }
		}
		    
		    //if record not fuund then find using registration number
		    /*
		    3) registration ?? 
             --- 3.1) (number, registration date, council)
             --- 3.2) registration number + dob + (first + middle + last)
             --- 3.3) registration number + dob
            */
            if( count($data['registration']) > 0){
                //3.1(number, registration_date, council)
                foreach($data['registration'] as $key=> $registration){
                    I2CE::raiseMessage("find persion by registration". $registration['registration_number']);
                    if( isset($registration['council'] ) &&  isset($registration['registration_number']) && isset($registration['registration_date'])){
                        
                        $person_id = $this->findPersonByReg($registration['council'], $registration['registration_number'], $registration['registration_date'], false);
                        
            	        if( is_array($person_id) and count($person_id) == 1 ){
            	            writeLog($this->person_log_record_id, "Record Found by registration number 3.1 ". $registration['registration_number'] );
            	            //update recod
            	            
            	            return $this->createPerson($data, current($person_id), true );
            	            break;
            	        }else if($person_id == -1){
            	            return -1;
            	        }
                    }
                }
            
                //(3.2) registration number + dob + (first + middle + last)
                foreach($data['registration'] as $key=> $registration){
                    if( isset($registration['council'] ) &&  isset($registration['registration_number'])){
                            
                        $person_id = $this->findPersonByRegDOBName($registration['registration_number'], $registration['council'], $data, $with_name=true);
                        
                        if( $person_id ){
            	            writeLog($this->person_log_record_id, "Record Found by registration number 3.2 ". $registration['registration_number'] );
            	            //update recod
            	            return $this->createPerson($data, $person_id, true );
            	            break;
            	        }
                    }
                }
                
                // (3.3) registration number + dob
                foreach($data['registration'] as $key=> $registration){
                    $person_id = $this->findPersonByRegDOBName($registration['registration_number'], $registration['council'], $data, false);
                    if( $person_id ){
        	            writeLog($this->person_log_record_id, "Record Found by registration number 3.3 by registration and dob ". $registration['registration_number'] );
        	            //update recod
        	            return $this->createPerson($data, $person_id, true );
        	            break;
        	        }
                }
                
            
            
            
            I2CE::raiseMessage("Record Not found, Creating new record");
		    writeLog($this->person_log_record_id, "Record Not found, Creating new record");
		    //else create new person
		    return $this->createPerson($data, false, true); #third params for insert source
		
		
    		/*
    		$found_key = array_search('Citizenship', array_column($data['person_id'], 'id_type'));
    		
    		if( gettype($found_key) == 'integer'){
    			$pdata = $data['person_id'][$found_key];
    			//$id_number, $id_type
    			$name_ids = $this->findPersonByID($pdata);
    			
    			if($name_ids and count($name_ids) == 1){
    				return $this->createPerson($data, current($name_ids));
    			}
    		}
    		*/
		}else{
			//ignore this data
			//create log
			I2CE::raiseMessage("Ignore record, there is no persion_id". json_encode($data));
			writeLog($this->person_log_record_id, "Ignore record, there is no persion_id");
			return null;
		}
	}
	
	public function findPersonByRegDOBName($registration_number, $council, $data, $with_name = true){
        
        $persons = $this->findPersonByReg($council, $registration_number, '');
        
        if( count($persons) >= 1 ){
            $persons = $this->checkPersonDOB($persons, $data['demographic']['birth_date']);
            
            if( count($persons) > 1) {
                I2CE::raiseMessage("Ignore record, Record found more than 1". json_encode($data));
                writeLog($this->person_log_record_id, "Ignore record, Record found more than 1");
                
                return -1;
            }
            
            if( !$persons ){
                I2CE::raiseMessage("Record not found by dob");
                writeLog($this->person_log_record_id, "Record not found by dob");
                return false;
            }
        }
        
        if( !is_array($persons) or count($persons) < 1 ) return;
        
        $person = current($persons);
        // print_r($person); exit;
        
        
        if( $with_name ){
            $operand = array(
                0=>array(
    					'operator'=>'FIELD_LIMIT',
    					'field'=>'id',
    					'style'=>'equals',
    					'data'=>array( 'value'=> str_replace("person|","",$person) )
    				),
    		
               1=>array(
    					'operator'=>'FIELD_LIMIT',
    					'field'=>'firstname',
    					'style'=>'equals',
    					'data'=>array( 'value'=>trim($data['firstname']) )
    				),
    			
    			2=>array(
    					'operator'=>'FIELD_LIMIT',
    					'field'=>'surname',
    					'style'=>'equals',
    					'data'=>array( 'value'=>trim($data['surname']) )
    				)
            );
	    
    	    if( trim($data['middle_name']) ){
    	        $operand[3] = array(
        						'operator'=>'FIELD_LIMIT',
        						'field'=>'middle_name',
        						'style'=>'equals',
        						'data'=>array( 'value'=>trim($data['middle_name']) )
    					       );
    	    }
            
    	    //search person by identification
    		$whereIdentification = array(
    			'operator'=>'AND',
    			'operand'=>$operand
    		);
    
            I2CE::raiseMessage("Find record by dob and name");
            
            $person_ids = I2CE_FormStorage::listFields('person', array('id'), false, $whereIdentification);
    		
    		if( is_array($person_ids) and count($person_ids) > 1){
    		    return $person_ids;
    		}
    		
    		if( is_array($person_ids) and count($person_ids) > 0 ){
    		    $person = current($person_ids);
    		    return "person|".$person['id'];
    		}
    		
        }else{
            return $person;
        }
        
        return false;
	}
	
	public function checkByRegistrationNumber($person, $reg_no){
	    $operand = array(
	           	0=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'parent',
						'style'=>'lowerequals',
						'data'=>array( 'value'=>trim($person) )
						),
				
				1=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'registration_number',
						'style'=>'equals',
						'data'=>array( 'value'=>trim($reg_no) )
					)
	        );
	    
	    
			//search person by identification
			$whereIdentification = array(
				'operator'=>'AND',
				'operand'=>$operand
			);

			$person_ids = I2CE_FormStorage::listFields('registration', array('parent', 'id'), false, $whereIdentification);
			
			if( count($person_ids) > 0 ){
			    $person = current($person_ids);
			    return $person['parent'];
			}
	}
	
	public function checkPersonDOB($person_id, $dob){
	    $person_id = (array) $person_id;
	    $operand = array(
	           	0=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'parent',
						'style'=>'in', // add in
						'data'=>array( 'value'=>$person_id )
						),
				
				1=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'birth_date',
						'style'=>'equals',
						'data'=>array( 'value'=>trim($dob) )
					)
	        );
	    
	    
			//search person by identification
			$whereIdentification = array(
				'operator'=>'AND',
				'operand'=>$operand
			);

			$person_ids = I2CE_FormStorage::listFields('demographic', array('parent', 'id'), false, $whereIdentification);
			
			if( count($person_ids) > 0){
			    $arr = array();
				foreach ($person_ids as $key => $value) {
					$arr[] = $value['parent'];
				}
				if(count($arr) > 0)
				    return $arr;
				
				return false;
			}
			
			return false;
	    
	}
	
	public function findPersonByReg($council, $registration_number, $registration_date, $createRecord = false){
		if($council && $registration_number){
			$council = $this->councilExists($council);
			$operand =  array(
						0=>array(
								'operator'=>'FIELD_LIMIT',
								'field'=>'council',
								'style'=>'equals',
								'data'=>array( 'value'=>trim("council|".$council) )
								),
						1=>array(
								'operator'=>'FIELD_LIMIT',
								'field'=>'registration_number',
								'style'=>'equals',
								'data'=>array( 'value'=>trim($registration_number) )
								)
						);
			if(strlen($registration_date) > 0){
				$operand[2] =array(
								'operator'=>'FIELD_LIMIT',
								'field'=>'registration_date',
								'style'=>'equals',
								'data'=>array( 'value'=>trim($registration_date) )
								);
			}
			
			//search person by identification
			$whereIdentification = array(
				'operator'=>'AND',
				'operand'=>$operand
				);

			$person_ids = I2CE_FormStorage::listFields('registration', array('parent'), false, $whereIdentification);
			
			if( ( count($person_ids) == 0 )  & $createRecord ){
				$dt = $this->mapped_data;
				//we need to create this person
				$personObj = $this->ff->createContainer('person');
				$personObj->firstname = trim($dt["firstname"]);
				$personObj->middle_name = trim($dt["middle_name"]);
				$personObj->surname = trim($dt["surname"]);
				$personObj->nationality = array('country', $this->countryExists($dt['nationality']) );
				$personObj->residence = array('district', trim($this->districtExists($dt['residence']['district'])));
				$personId = $this->save($personObj);
				return 'person|'.$personId;
			}
			else{
				$arr = array();
				foreach ($person_ids as $key => $value) {
					$arr[] = $value['parent'];
				}
				if(count($arr) > 0)
				    return $arr;
				return false;
			}
		}
	}
	public function findPersonByID($data, $createRecord = true){
	    
		if(count($data) > 0 ){
		    $id_number = $data['id_num'];
		    $id_type = $data['id_type'];
		    $date = $data['issue_date'];
		    
		    //search person by identification
			$whereIdentification = array(
				'operator'=>'AND',
				'operand'=>array(
						0=>array(
								'operator'=>'FIELD_LIMIT',
								'field'=>'id_num',
								'style'=>'equals',
								'data'=>array( 'value'=>trim($id_number) )
						),
						1=>array(
								'operator'=>'FIELD_LIMIT',
								'field'=>'id_type',
								'style'=>'equals',
								'data'=>array( 'value'=>trim( 'id_type|'.$this->idTypeExists( $id_type )) )
						),
						2=>array(
								'operator'=>'FIELD_LIMIT',
								'field'=>'issue_date',
								'style'=>'equals',
								'data'=>array( 'value'=>trim($date))
						),
						3=>array(
								'operator'=>'FIELD_LIMIT',
								'field'=>'place',
								'style'=>'equals',
								'data'=>array( 'value'=>trim($data['place']))
						)
						
						
					)
				);
			$person_ids = I2CE_FormStorage::listFields('person_id', array('parent'), false, $whereIdentification);
// 			print_r($whereIdentification);
// 			print_r($person_ids);
// 			exit;
			if( ( count($person_ids) == 0 )  ){
				return false;
			}
			else{
				$arr = array();
				foreach ($person_ids as $key => $value) {
					$arr[] = $value['parent'];
				}
				return $arr;
			}
		}
	}
	
	
	
	protected function updatePersonRegistration($personId, $registration = array() ){
    	if( ($personObj = $this->ff->createContainer($personId)) instanceof iHRIS_Person ){
    		foreach( $registration as $index=>$data){
    		    
    		    if( empty(trim($data['registration_number']) ) ) continue;
    		    
    		    $reg = $this->findPersonRegistration($personId, $data);
    		    if( $reg ){
    		        $formObj = $this->ff->createContainer('registration|'.$reg);
    		    }else{
    			    $formObj = $this->ff->createContainer('registration');
    		    }
    		    
    			$formObj->getField('registration_number')->setValue(trim($data['registration_number']));
    			if(isset($data['registration_date']) && strlen(trim($data['registration_date']))>0){
    				$formObj->getField('registration_date')->setValue(I2CE_Date::fromDB(trim($data['registration_date'])));
    			}
                $formObj->getField('council')->setValue(array('council', $this->councilExists($data['council']) )) ;
                $formObj->getField('registration_category')->setValue(array('document_category', $this->registrationCategoryExists($data['registration_category']) )) ;

    			$formObj->setParent($personId);
    			$this->save($formObj);
    		}
    	}
    }
    
    protected function updatePersonEducation($personId, $education = array()){
        if(count($education) == 0 ) return;
        
        if( ($personObj = $this->ff->createContainer($personId)) instanceof iHRIS_Person ){
            foreach( $education as $index=>$data){
                if( empty(trim($data['institution'])) and empty(trim($data['degree']))) continue;
                
                $found = $this->findPersonEducation($personId, $data );
                if( $found ){
                    $formObj = $this->ff->createContainer('education|'.$found);
                }else{
                    $formObj = $this->ff->createContainer('education');
                }
                
                $formObj->getField('country')->setValue(array('country', $this->countryExists($data['country']) ) );
                $formObj->getField('institution')->setValue(array('institution', $this->institutionExists($data['institution']) ) );
                
                $formObj->getField('degree')->setValue(array('degree', $this->degreeExistsOrCreate($data['degree_detail']) ) );
                
                
                $formObj->getField('started_date')->setValue(I2CE_Date::fromDB(trim($data['started_date'])) );
                $formObj->getField('completed_date')->setValue(I2CE_Date::fromDB(trim($data['completed_date'])) );
                
                $formObj->getField("major")->setField(trim($data['major']) );
                
                $formObj->setParent($personId);
                $this->save($formObj);
            }
        }
    }
    
    protected function findPersonEducation($personId, $education){
	    if(!$personId or count($education) == 0 ){
		    return false;
		}
		
		$country_id = $this->countryExists($education['country']);
		$instituation = $this->institutionExists($education['institution']);
		$degree = $this->degreeExistsOrCreate($education['degree_detail']);
		
        //search person by names
		$where = array(
			'operator'=>'AND',
			'operand'=>array(
				0=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'parent',
						'style'=>'lowerequals',
						'data'=>array(
								'value'=>trim($personId)
								)
						),
				1=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'country',
						'style'=>'lowerequals',
						'data'=>array( 'value'=>trim('country|'.$country_id) )
						),
				2=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'institution',
						'style'=>'lowerequals',
						'data'=>array( 'value'=>trim('institution|'. $instituation) ) 
						),
			    3=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'degree',
						'style'=>'lowerequals',
						'data'=>array( 'value'=>trim( 'degree|'. $degree ) )
						)
				
				)
			);
		
		$refId = I2CE_FormStorage::listFields('education', array('id'), false, $where);
		
		if( $refId and count($refId) > 0){
		    return current(current($refId));
		}
		return false;
		
	}
	
	
	protected function findPersonRegistration($personId, $data){
	    if(!$personId or count($data) == 0 ){
		    return false;
		}
		
		$council = $this->councilExists($data['council']);
		$reg_cat = $this->registrationCategoryExists($data['registration_category']);
		
        //search person by names
		$where = array(
			'operator'=>'AND',
			'operand'=>array(
				0=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'parent',
						'style'=>'lowerequals',
						'data'=>array(
								'value'=>trim($personId)
								)
						),
				1=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'council',
						'style'=>'lowerequals',
						'data'=>array( 'value'=>trim('council|'. $council) ) 
						),
			    2=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'registration_category',
						'style'=>'lowerequals',
						'data'=>array( 'value'=>trim( 'document_category|'. $reg_cat ) )
						),
				3=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'registration_number',
						'style'=>'lowerequals',
						'data'=>array( 'value'=>trim( $data['registration_number'] ))
						)
				
				)
			);
		$refId = I2CE_FormStorage::listFields('registration', array('id'), false, $where);
		
	    if( $refId and count($refId) > 0){
		    return current(current($refId));
		}
		return false;
		
	}
	
    public function positionExists($data){
		//creating position

		$formObj = $this->ff->createContainer('position');
		$formObj->getField('job')->setValue(array('job', $this->jobExists($data['job']) ) );
		$formObj->getField('title')->setValue(trim($data['title']));
		$formObj->getField('facility')->setValue(array('facility', $this->facilityExistsOrCreate($data['facility_detail']) ) );
		return $this->save($formObj);
		//return $this->checkTitleExists('iHRIS_PersonPosition', $title);
	}
    
    
    public function getPositionId($data, $status = "open"){
        if(empty(trim($data['job'])) || empty(trim($data['title']) ) || empty(trim($data['facility']) )) return;
        
		$jobId = $this->jobExists($data['job']);
		$facilityId = $this->facilityExistsOrCreate($data['facility_detail']);
		//search position by title
		$wherePosition = array(
			'operator'=>'AND',
			'operand'=>array(
				0=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'title',
						'style'=>'lowerequals',
						'data'=>array('value'=>trim($data['title']) )
						),
				1=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'job',
						'style'=>'lowerequals',
						'data'=>array(
								'value'=>trim("job|".$jobId)
								)
						),
				2=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'facility',
						'style'=>'lowerequals',
						'data'=>array(
								'value'=>trim("facility|".$facilityId)
								)
						)
			
				)
			);
			
		$position = I2CE_FormStorage::search('position', false, $wherePosition);
		
		I2CE::raiseMessage("Position COUNT = ".count($position));
		if( count($position) > 1 ){
			return current($position);
		}
		else if(count($position) == 1){
			return $position[0];
		}
		elseif( (count($position) == 0) and !empty(trim($data['title'])) ){
			//we need to create this position
			$formObjPos = $this->ff->createContainer('position');
			$formObjPos->getField('job')->setValue(array('job', $jobId));
			$code = trim($data['code']);
			if(0 == strlen($code))
				$code = trim($data['title']);
				
			$formObjPos->getField('code')->setValue($code);
			$formObjPos->getField('title')->setValue(trim($data['title']));
			$formObjPos->getField('facility')->setValue(array('facility', $facilityId));
			$formObjPos->getField('status')->setValue(array('position_status','open'));
			
			$formObjPos->getField('proposed_hiring_date')->setValue(I2CE_Date::fromDB(trim($data['start_date'])));
			$formObjPos->getField('posted_date')->setValue(I2CE_Date::fromDB(trim($data['start_date'])));
			
			$positionId = $this->save($formObjPos);
// 			print_r($positionId); exit;
			return $positionId;
		}
	}

    public function findPersonPosition($personId, $positionId ){
        
        $wherePosition = array(
			'operator'=>'AND',
			'operand'=>array(
				0=>array(
						'operator'=>'FIELD_LIMIT',
						'field' => 'parent',
						'style'=>'lowerequals',
						'data'=>array('value'=>trim($personId) )
						),
				1=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'position',
						'style'=>'lowerequals',
						'data'=>array( 'value'=>trim($positionId) )
						)
				)
		);
			
		$position = I2CE_FormStorage::search('person_position', false, $wherePosition);
		
		if( count($position) > 1 ){
			return current($position);
		}
		else if(count($position) == 1){
			return $position[0];
		}else{
		    return null;
		}
    }
    
    protected function updatePersonPosition($personId, $position = array() ){
        
        if(empty($position['title']) && empty($position['job'])) return;
        
		if( ($personObj = $this->ff->createContainer($personId)) instanceof iHRIS_Person ){
			//set person's position
			$positionId = $this->getPositionId($position);
			if( $positionId == null ) return;
			
			$pos = 'position|'.$positionId;
			I2CE::raiseMessage("Position ID = ".$pos);
			
			//now udpate the status of used position.
			$formObjP = $this->ff->createContainer($pos);
			$formObjP->setId($pos);
			I2CE::raiseMessage("Get Position ID = ".$formObjP->getId());
			$formObjP->statusOnly();
			$formObjP->getField('status')->setValue(array('position_status','closed'));
			$this->save($formObjP);

            $position_id = $this->findPersonPosition($personId, $pos);
            
            if( $position_id ){
                $formObjPP = $this->ff->createContainer( 'person_position|'.$position_id );
            }else{
                $formObjPP = $this->ff->createContainer( 'person_position' );
            }
			
			$formObjPP->getField('start_date')->setValue(I2CE_Date::fromDB(trim($position['start_date'])));
			$formObjPP->setParent($personId);
			
			$formObjPP->getField('position')->setValue(array('position', $positionId));
			$this->save($formObjPP);
			

			//save the salary
			$formObjS = $this->ff->createContainer('salary');
			$salary =  array("currency|".$this->currencyExists($position['currency']) , $position["salary"]);
			$formObjS->getField('salary')->setValue($salary);
			$formObjS->getField('start_date')->setValue(I2CE_Date::fromDB(trim($position['start_date'])));
			$formObjS->setParent($personId);
			$this->save($formObjS);
		}
	}


}


/*********************************************
*
*      Execute!
*
*********************************************/
//ini_set('memory_limit','3000MB');
echo "<pre>";
if (count($arg_files) < 1) {
	usage("Please specify the name of a JSON to process");
}

$id = null;

//reset($arg_files);
// $file = current($arg_files);
// $file = "upload/3/3/abc.txt";
$file = $arg_files[0];

if(count($arg_files) == 2 )
    $id = $arg_files[1];
if($file[0] == '/') {
		$file = realpath($file);
} else {
		$file = realpath($dir. '/' . $file);
}

if (!is_readable(trim($file))) {
	usage("Please specify the name of a Json to import: " . $file . " is not readable");
}

I2CE::raiseMessage("Loading from $file");

// echo ")";exit();
$processor = new PersonalData_Import($file, false, $id);
$processor->run();

echo "Processing Statistics:\n";
print_r( $processor->getStats());




# Local Variables:
# mode: php
# c-default-style: "bsd"
# indent-tabs-mode: nil
# c-basic-offset: 4
# End:
