#!/usr/bin/php
<?php
/*
 * © Copyright 2007, 2008 IntraHealth International, Inc.
 *
 * This File is part of iHRIS
 *
 * iHRIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * The page wrangler
 *
 * This page loads the main HTML template for the home page of the site.
 * @package iHRIS
 * @subpackage DemoManage
 * @access public
 * @author Sovello Hildebrand sovellohpmgani@gmail.com
 * @copyright Copyright &copy; 2007, 2008-2013 IntraHealth International, Inc.
 * @version 4.6.0
 */
/*
php import_position_sanction_count.php ./data/position/position_2000_1.json
php import_position_sanction_count.php ./data/position/position_xxxx.json
php import_data.php /path/to/your/excel_sheet.csv

*/
require_once("./import_base.php");

class JobData_Import extends Processor{

		public function __construct($file) {
			parent::__construct($file);
		}

		//map headers from the spreadsheet
		//what you do here is change the values on the right to match what you have on the spreadsheet. comment out lines that are not in the spreadsheet
		//the values of the left are used by the script to refer to the spreadsheet columns on the right of this array.
		//the order of the columns in the spreadsheet doesn't matter
		//{"job":"job","title":"title","pos_type":"pos_type","facility":"facility"}

		//problem : date field, application and exam
		protected function getExpectedHeaders(){
			$a = array(
				"id" => "id",
				"parent" => "parent",
				"application_date" => 'application_date',
				"exam_date"	=> "exam_date",
				"exam_number" => 'exam_number',
				"materials_approved" => "materials_approved",
				"materials_received" => 'materials_received',
				"results"	=> 'results',
				"try"	=> 'try'
			);
			var_dump($a);
			return $a;
		}

		//in this part comment out if you are not adding any data for that specific item.
		//for example if there is no nextofkin data in the spreadsheet,
		//comment out line 108 by preceding it with double-slasses as in this line
		//remember to also comment out all lines in the getExpectedHeaders() function lines 46-63
		protected function _processRow(){
			$details = $this->mapped_data;
			$this->update($details);
			// I2CE::raiseMessage("No of records imported = $addedRows");
		}
		public function update($data){
			//search if data exits based on code
			$id= $this->checkExists("exam", 'id', $data['id'], false); 
			//if not exists then insert
			if( $id == 0 || $id == null){
				echo "Inside the code \n";
				$formObj = $this->ff->createContainer('exam');
			}else{
				//update data
				echo "inside the update";
				echo $id; 
				$formObj = $this->ff->createContainer('exam|'.$id);
			}
			$formObj->getField('parent')->setValue(trim($data['parent']));
			$formObj->getField('application_date')->setValue(I2CE_Date::fromDB(trim($data['application_date'])));
			$formObj->getField('exam_date')->setValue(I2CE_Date::fromDB(trim($data['exam_date'])));
			$formObj->getField('exam_number')->setValue(trim($data['exam_number']));
			$formObj->getField('materials_approved')->setValue(trim($data['materials_approved']));
			$formObj->getField('materials_received')->setValue(trim($data['materials_received']));
			$formObj->getField('results')->setValue(trim($data['results']));
			$formObj->getField('try')->setValue(trim($data['try']));
			$positionId = $this->save($formObj);
		}
		
		/****************************************************************************
		 *                                                                          *
		 *   DON'T EDIT BEYOND THIS POINT UNLESS YOU KNOW WHAT YOU WANT TO ACHIEVE  *
		 *                                                                          *
		 ****************************************************************************/
		
		public function jobExists($title){
			return $this->checkTitleExists('job', $title);
		}
		public function facilityExists($name){
			return $this->checkNameExists('facility', $name);
		}
		public function districtExists($name){
			return $this->checkNameExists('district', $name);
		}
}


/*********************************************
*
*      Execute!
*
*********************************************/

//ini_set('memory_limit','3000MB');


if (count($arg_files) != 1) {
		usage("Please specify the name of a JSON-file to process");
}

reset($arg_files);
$file = current($arg_files);
if($file[0] == '/') {
		$file = realpath($file);
} else {
		$file = realpath($dir. '/' . $file);
}
if (!is_readable($file)) {
		usage("Please specify the name of a JSON-file to import: " . $file . " is not readable");
}

I2CE::raiseMessage("Loading from $file");


$processor = new JobData_Import($file);
$processor->run();

echo "Processing Statistics:\n";
print_r( $processor->getStats());


# Local Variables:
# mode: php
# c-default-style: "bsd"
# indent-tabs-mode: nil
# c-basic-offset: 4
# End:
