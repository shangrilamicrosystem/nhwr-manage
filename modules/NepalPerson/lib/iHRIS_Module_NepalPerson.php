<?php
/**
 * Class iHRIS_Module_$moduleName
 *
 * @access public
 */
class iHRIS_Module_NepalPerson extends I2CE_Module {
    /**
     * Return the array of hooks available in this module.
     * @return array
     */
    public static function getHooks() {
        return array(
                'validate_form_person' => 'validate_form_person', 
            );
    }

    /**
     * Validate the $form form.
     * @param I2CE_Form $form
     */
    public function validate_form_person( $form ) {
        $uuid = uniqid("nhwr-");
        if( !$form->uuid )
            $form->uuid = $uuid;
    }

}
# Local Variables:
# mode: php
# c-default-style: "bsd"
# indent-tabs-mode: nil
# c-basic-offset: 4
# End:
