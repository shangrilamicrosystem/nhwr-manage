<?php
class iHRIS_Module_NepalPersonReferredBy extends I2CE_Module {
    public static function getMethods() {
        return array(
            'iHRIS_PageView->action_nepal_person_refrences' => 'action_nepal_person_refrences',
            );
    }
 
 
    public function action_nepal_person_refrences($obj) {
        if (!$obj instanceof iHRIS_PageView) {
            return;
        }
        return $obj->addChildForms('nepal_person_refrences', 'siteContent');
    }
}
?>