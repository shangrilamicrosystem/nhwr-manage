<?php
class iHRIS_Module_NepalPersonSource extends I2CE_Module {
    public static function getMethods() {
        return array(
            'iHRIS_PageView->action_nepal_person_source' => 'action_nepal_person_source',
            );
    }
 
 
    public function action_nepal_person_source($obj) {
        if (!$obj instanceof iHRIS_PageView) {
            return;
        }
        return $obj->addChildForms('nepal_person_source', 'siteContent');
    }
}
?>