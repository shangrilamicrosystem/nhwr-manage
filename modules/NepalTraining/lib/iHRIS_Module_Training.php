<?php
class iHRIS_Module_Training extends I2CE_Module {
    public static function getMethods() {
        return array(
            'iHRIS_PageView->action_person_training' => 'action_person_training',
            );
    }
 
 
    public function action_person_training($obj) {
        if (!$obj instanceof iHRIS_PageView) {
            return;
        }
        return $obj->addChildForms('person_training', 'siteContent');
    }
}
?>