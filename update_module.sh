#!/bin/bash
echo "call ihris-update"
php /var/lib/iHRIS/sites/manage-nhwr/pages/index.php --update=1 --force-restart=1
echo "Restarting memcached server"
sudo service memcached restart

echo "Restarting apache2 server"
sudo service apache2 restart
