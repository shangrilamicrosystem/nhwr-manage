<?php
require_once "form_base.php";

class form_person_nfo extends form_base{
	function __construct()
	{
		$this->parent_form = "person";
	}

	function prepareSql($parent, $param){
		if(strlen($parent) == 0)
			return null;
		$p_arr = explode("|", $parent);
		var_dump($p_arr);
		$parent_id = $p_arr[1];
		$sql = "
		SELECT  fo.name as form, f.name, coalesce(le.string_value, le.integer_value, le.text_value, le.date_value) value,le.string_value, le.integer_value
		FROM `form_field` ff
			 left join field f on ff.field = f.id
			 left join last_entry le on ff.id = le.form_field
			 left join record r on le.record = r.id
			 left join form fo on ff.form = fo.id
		where r.parent_form = 'person'
			and r.parent_id = $parent_id
		
		";
		return $sql;
	}

}