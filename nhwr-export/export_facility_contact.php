<?php
error_reporting(E_ALL); ini_set('display_errors', 1);
require_once 'export_base.php';
require_once dirname(__FILE__)."/i2ce/form_base.php";

$sql = 'SELECT * FROM `hippo_facility_contact`';
$db = new DB();
$db->connect();
class export_region extends form_base{
}

$frm = new export_region();
$records = $frm->fetch_data($sql);
$all_data = json_encode($records, JSON_PRETTY_PRINT);

file_put_contents(dirname(__FILE__)."/data/facility_contact.json", $all_data);

echo "complete!!\n";
$db->close();