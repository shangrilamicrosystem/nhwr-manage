<?php
error_reporting(E_ALL); ini_set('display_errors', 1);
require_once "export_base.php";
require_once "i2ce/master/country.php";
echo "<pre>";
$db = new DB();
$db->connect();

$frm = new form_master_country();
$record = $frm->getRecords();
$all_data = json_encode($record, JSON_PRETTY_PRINT);

file_put_contents(dirname(__FILE__)."/data/country.json", $all_data);

$db->close();