<?php
$dir = getcwd();
chdir("../pages");
$i2ce_site_user_access_init = null;
$i2ce_site_user_database = null;
require_once( getcwd() . DIRECTORY_SEPARATOR . 'config.values.php');

$local_config = getcwd() . DIRECTORY_SEPARATOR .'local' . DIRECTORY_SEPARATOR . 'config.values.php';
if (file_exists($local_config)) {
    require_once($local_config);
}

if(!isset($i2ce_site_i2ce_path) || !is_dir($i2ce_site_i2ce_path)) {
    echo "Please set the \$i2ce_site_i2ce_path in $local_config";
    exit(55);
}

class form_person_data{
    public $person;
	function __construct($person_id)
	{
        $this->person = $person_id;
    }

    function getPersonIds(){

        $person_id = $this->person;
        //search person by names
        $wherePerson = array(
            'operator'=>'AND',
            'operand'=>array(
                0=>array(
                        'operator'=>'FIELD_LIMIT',
                        'field'=>'parent',
                        'style'=>'lowerequals',
                        'data'=>array(
                                'value'=>trim($person_id)
                                )
                        )
                )
            );
    
        $person_ids = I2CE_FormStorage::search('person_id', false, $wherePerson);
        
        
        $arr = array();
        foreach ($person_ids as $key => $value) {
            $id = 'person_id|'.$value;
            
            $ff = I2CE_FormFactory::instance();
            $personObj = $ff->createContainer($id);
            $personObj->populate();

            //id type
            $id_type = $personObj->getField('id_type')->getDBValue();
            $id_type = $ff->createContainer($id_type);
            $id_type->populate();
            $id_name = $id_type->getField("name")->getDBValue(); 
            
            //country
            $country = $personObj->getField('country')->getDBValue();
            $country = $ff->createContainer($country);
            $country->populate();
            $country_name = $country->getField("name")->getDBValue(); 


            $info = array();
            $info = array(
                    'id_type' => $id_name,
                    'id_num' => $personObj->getField('id_num')->getDBValue(),
                    'issue_date'  => $personObj->getField('issue_date')->getDBValue(),
                    'issue_date'  => $personObj->getField('issue_date')->getDBValue(),
                    'expiration_date'  => $personObj->getField('expiration_date')->getDBValue(),
                    'country'  => $country_name,
                    'place'  => $personObj->getField('place')->getDBValue(),
            );

            $arr[] = $info;
        }
        return $arr;
        
        
    }

    public function getParentInfo() {
        $person_id = $this->person;
        //search person by names
        $wherePerson = array(
            'operator'=>'AND',
            'operand'=>array(
                0=>array(
                        'operator'=>'FIELD_LIMIT',
                        'field'=>'parent',
                        'style'=>'lowerequals',
                        'data'=>array(
                                'value'=>trim($person_id)
                                )
                        )
                )
            );
    
        $id = I2CE_FormStorage::search('person_parent_info', false, $wherePerson);
        if( !isset($id) or count($id) ==0 ){
            return array();
        }
        $id = "person_parent_info|".$id[0];

        $ff = I2CE_FormFactory::instance();
        $personObj = $ff->createContainer($id);
        $personObj->populate();
        
        return array(
            'father_firstname' => $personObj->getField('father_firstname')->getDBValue(),
            'father_middlename' => $personObj->getField('father_middlename')->getDBValue(),
            'father_surname'  => $personObj->getField('father_surname')->getDBValue(),


            'mother_firstname' => $personObj->getField('mother_firstname')->getDBValue(),
            'mother_middlename' => $personObj->getField('mother_middlename')->getDBValue(),
            'mother_surname'  => $personObj->getField('mother_surname')->getDBValue(),

            'husband_firstname' => $personObj->getField('husband_firstname')->getDBValue(),
            'husband_middlename' => $personObj->getField('husband_middlename')->getDBValue(),
            'husband_surname'  => $personObj->getField('husband_surname')->getDBValue(),

        );
        
    }

    public function getDemographic() {
        $person_id = $this->person;
        //search person by names
        $wherePerson = array(
            'operator'=>'AND',
            'operand'=>array(
                0=>array(
                        'operator'=>'FIELD_LIMIT',
                        'field'=>'parent',
                        'style'=>'lowerequals',
                        'data'=>array(
                                'value'=>trim($person_id)
                                )
                        )
                )
            );
    
        $id = I2CE_FormStorage::search('demographic', false, $wherePerson);
        if( !isset($id) or count($id) == 0 ){
            return array();
        }
        $id = "demographic|".$id[0];

        $ff = I2CE_FormFactory::instance();
        $personObj = $ff->createContainer($id);
        $personObj->populate();

        //gender
        $gender = $personObj->getField('gender')->getDBValue();
        $gender = $ff->createContainer($gender);
        $gender->populate();
        $gender = $gender->getField("name")->getDBValue(); 
        
        //marital_status
        $marital_status = $personObj->getField('marital_status')->getDBValue();
        if( $marital_status ){
            $marital_status = $ff->createContainer($marital_status);
            $marital_status->populate();
            $marital_status = $marital_status->getField("name")->getDBValue(); 
        } else {
            $marital_status = "";
        }
        return array(
            'birth_date' => $personObj->getField('birth_date')->getDBValue(),
            'gender' => $gender,
            'marital_status'  => $marital_status,
            'dependents' => $personObj->getField('dependents')->getDBValue()
        );
        
    }

    public function getPersonalContact() {
        $person_id = $this->person;
        //search person by names
        $wherePerson = array(
            'operator'=>'AND',
            'operand'=>array(
                0=>array(
                        'operator'=>'FIELD_LIMIT',
                        'field'=>'parent',
                        'style'=>'lowerequals',
                        'data'=>array( 'value'=>trim($person_id) )
                        )
                )
            );
    
        $id = I2CE_FormStorage::search('person_contact_personal', false, $wherePerson);
        if( !isset($id) or count($id) == 0 ){
            return array();
        }
        $id = "person_contact_personal|".$id[0];

        $ff = I2CE_FormFactory::instance();
        $personObj = $ff->createContainer($id);
        $personObj->populate();

        return array(
            'telephone' => $personObj->getField('telephone')->getDBValue(),
            'mobile_phone' => $personObj->getField('mobile_phone')->getDBValue(),
            'email' => $personObj->getField('email')->getDBValue()
        );

    }

    //person_contact_emergency
    public function getEmergencyContact() {
        $person_id = $this->person;
        //search person by names
        $wherePerson = array(
            'operator'=>'AND',
            'operand'=>array(
                0=>array(
                        'operator'=>'FIELD_LIMIT',
                        'field'=>'parent',
                        'style'=>'lowerequals',
                        'data'=>array( 'value'=>trim($person_id) )
                        )
                )
            );
    
        $id = I2CE_FormStorage::search('person_contact_emergency', false, $wherePerson);
        
        if( !$id or count($id) == 0 ){
            return array();
        }
        $id = "person_contact_emergency|".$id[0];

        $ff = I2CE_FormFactory::instance();
        $personObj = $ff->createContainer($id);
        $personObj->populate();
        
        
        return array(
            'firstname' => $personObj->getField('firstname')->getDBValue(),
            'middlename' => $personObj->getField('middlename')->getDBValue(),
            'surname' => $personObj->getField('surname')->getDBValue(),
            'telephone' => $personObj->getField('telephone')->getDBValue(),
            'mobile_phone' => $personObj->getField('mobile_phone')->getDBValue(),
            'alt_telephone' => $personObj->getField('alt_telephone')->getDBValue(),
            'email' => $personObj->getField('email')->getDBValue()
        );

    }
    
    //getRegistration
    public function getRegistration() {
        $person_id = $this->person;
        //search person by names
        $wherePerson = array(
            'operator'=>'AND',
            'operand'=>array(
                0=>array(
                        'operator'=>'FIELD_LIMIT',
                        'field'=>'parent',
                        'style'=>'lowerequals',
                        'data'=>array( 'value'=>trim($person_id) )
                        )
                )
            );
    
        $id = I2CE_FormStorage::search('registration', false, $wherePerson);
        
        if( !$id or count($id) == 0 ){
            return array();
        }
        $id = "registration|".$id[0];

        $ff = I2CE_FormFactory::instance();
        $personObj = $ff->createContainer($id);
        $personObj->populate();
        
        $council = $personObj->getField('council')->getDBValue();
        $council = $ff->createContainer($council);
        $council->populate();
        $council = $council->getField("name")->getDBValue(); 
        
        
        //category 
        $category = $personObj->getField('registration_category')->getDBValue();
        $category = $ff->createContainer($category);
        $category->populate();
        $category = $category->getField("name")->getDBValue();
        
        return array(
            'council' => $council,
            'registration_number' => $personObj->getField('registration_number')->getDBValue(),
            'registration_category' => $category,
            'registration_date' => $personObj->getField('registration_date')->getDBValue(),
            'license_number' => $personObj->getField('license_number')->getDBValue(),
            'license_expiration' => $personObj->getField('license_expiration')->getDBValue()
        );

    }

    //nepal_training
    public function getNepalTraining(){
        $person_id = $this->person;
        //search person by names
        $wherePerson = array(
            'operator'=>'AND',
            'operand'=>array(
                0=>array(
                        'operator'=>'FIELD_LIMIT',
                        'field'=>'parent',
                        'style'=>'lowerequals',
                        'data'=>array(
                                'value'=>trim($person_id)
                                )
                        )
                )
            );
    
        $person_ids = I2CE_FormStorage::search('nepal_training', false, $wherePerson);
        
        
        $arr = array();
        foreach ($person_ids as $key => $value) {
            $id = 'nepal_training|'.$value;
            
            $ff = I2CE_FormFactory::instance();
            $personObj = $ff->createContainer($id);
            $personObj->populate();

            //country
            $country = $personObj->getField('country')->getDBValue();
            $country = $ff->createContainer($country);
            $country->populate();
            $country_name = $country->getField("name")->getDBValue(); 


            $info = array();
            $info = array(
                    'country' => $country_name,
                    'institution' => $personObj->getField('institution')->getDBValue(),
                    'location_other'  => $personObj->getField('location_other')->getDBValue(),
                    'started_date'  => $personObj->getField('started_date')->getDBValue(),
                    'completed_date'  => $personObj->getField('completed_date')->getDBValue(),
                    'minor'  => $personObj->getField('minor')->getDBValue(),
            );

            $arr[] = $info;
        }
        return $arr;
    }

    //nepal_referred_by
    public function getNepalReferredBy(){
        $person_id = $this->person;
        //search person by names
        $wherePerson = array(
            'operator'=>'AND',
            'operand'=>array(
                0=>array(
                        'operator'=>'FIELD_LIMIT',
                        'field'=>'parent',
                        'style'=>'lowerequals',
                        'data'=>array('value'=>trim($person_id))
                    )
                )
            );
    
        $person_ids = I2CE_FormStorage::search('nepal_referred_by', false, $wherePerson);
        
        
        $arr = array();
        foreach ($person_ids as $key => $value) {
            $id = 'nepal_referred_by|'.$value;
            
            $ff = I2CE_FormFactory::instance();
            $personObj = $ff->createContainer($id);
            $personObj->populate();

            $info = array();
            $info = array(
                    'registration_number' => $personObj->getField('registration_number')->getDBValue(),
                    'full_name'  => $personObj->getField('full_name')->getDBValue(),
                    'location_other'  => $personObj->getField('location_other')->getDBValue(),
                    'date'  => $personObj->getField('date')->getDBValue()
            );

            $arr[] = $info;
        }
        return $arr;
    }


    // common function 
    public function getName( $id ){
       $ff = I2CE_FormFactory::instance();
       $gender = $ff->createContainer($id);
       $gender->populate();
       
       return $gender->getField("name")->getDBValue();
    }
}