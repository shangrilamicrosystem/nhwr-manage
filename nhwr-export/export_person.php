<?php
error_reporting(E_ALL); ini_set('display_errors', 1);
require_once ("../nhwr-import/import_base.php");
require_once "export_base.php";
require_once "export_person_data.php";

$db = new DB();
$db->connect();

$get_last_export_date = "SELECT created_date FROM export_history ORDER BY id DESC LIMIT 1;";
$result = $db->conn->query($get_last_export_date);

$last_export_date = "2000-10-2";
while( $row = $result->fetch_assoc() ) {
    $last_export_date = $row['created_date'];
}


$latest_record_sql = <<<SQL
SELECT  distinct case when ff.form = 53 then r.id else r.parent_id end record_id
FROM `form_field` ff
     left join field f on ff.field = f.id
     left join last_entry le on ff.id = le.form_field
     left join record r on le.record = r.id
     left join form fo on ff.form = fo.id
where date_format(le.date, '%Y-%m-%d') > date_format('$last_export_date', '%Y-%m-%d') and date_format(le.date, '%Y-%m-%d') <= date_format(curdate(), '%Y-%m-%d')

and case when ff.form = 53 then r.id else r.parent_id end > 0
SQL;

// echo $latest_record_sql; exit;
//executing main sql related to person
$result = $db->conn->query($latest_record_sql);
$all_person_ids = array();
if($result->num_rows > 0){
    while( $row = $result->fetch_assoc() ) {
        $all_person_ids[] = "person|".$row['record_id'];
    }
}
print_r($all_person_ids);

echo $all_person_ids = join("','",$all_person_ids); 
// exit("exit");

$sql = <<<SQL
SELECT 
	p.id, p.uuid, p.firstname, p.middle_name, p.surname, p.maiden_name, p.othername, p.othername_2, p.othername_3,
	c.name citizenship, c2.name citizenship_secondary,
	c_c.name residence_country, c_r.name residence_region, c_d.name residence_district, c_v.name residence_vdc_mun,
	p.ward_number, p.street_name, p.house_number
FROM hippo_person p
	LEFT JOIN hippo_country c on p.nationality = c.id
	LEFT JOIN hippo_country c2 on p.nationality_secondary = c2.id
	LEFT JOIN hippo_county c_v on p.residence = c_v.id
	LEFT JOIN hippo_district c_d on c_v.district = c_d.id or p.residence = c_d.id
	LEFT JOIN hippo_region c_r on c_d.region = c_r.id or p.residence = c_r.id
	LEFT JOIN hippo_country c_c on c_r.country = c_c.id or p.residence = c_c.id

WHERE p.id in ('$all_person_ids')
SQL;
// echo $sql; exit;

// echo "<pre>";
function append_header(&$header, $record){
	foreach ($record as $key=>$value) {
		array_push($header, $key);
	}
}

//executing main sql related to person
$result = $db->conn->query($sql);
if($result->num_rows > 0){
	$cnt = 0;
	// output data of each row
	$all_person = array();
    while($row = $result->fetch_assoc()) {
		//now adding row data related to the person (person detail & demographics)
		$person = array();
		foreach ($row as $key=>$value) {
			$person[$key] = $value;
		}
// 		print_r($person); exit;
		// info class 
		$frm = new form_person_data($row["id"]);

// 		if($person['nationality']){
// 			$person['nationality'] = $frm->getName($person['nationality']);
// 		}

// 		if($person['residence']){
// 			$person['residence'] = $frm->getName( $person['residence'] );
// 		}
		
// 		if($person['nationality_secondary']){
// 			$person['nationality_secondary'] = $frm->getName( $person['nationality_secondary'] );
// 		}
		
			
		//parent info
		$person['person_parent_info'] = $frm->getParentInfo();
        
        // print_r($person); exit;
		//identification
		$person['person_id'] = $frm->getPersonIds();
		
		//demographic
		$person['demographic'] = $frm->getDemographic();

		//person_contact_personal
		$person['person_contact_personal'] = $frm->getPersonalContact();
		//person_contact_emergency
		$person['person_contact_emergency'] = $frm->getEmergencyContact();
		//nepal_training
		$person['registration'] = $frm->getRegistration();
		
		// nepal_refered_by
// 		$person['nepal_refered_by'] = $frm->getNepalReferredBy();

// 		print_r($person); exit;
		$all_person[] = $person;
		
		
// 		$cnt++;
// 		if($cnt == 20) break;
	}
	

	$all_data = json_encode($all_person, JSON_PRETTY_PRINT);
	// print_r($all_data); exit;
	file_put_contents(dirname(__FILE__)."/data/person.json", $all_data);
	
	$get_last_export_date = "INSERT INTO export_history (`no_record`) values (2) ;";
    $result = $db->conn->query($get_last_export_date);
    
	echo "complete!!\n";
	
	
}
else{
	echo "No records found.";
}
$db->close();
