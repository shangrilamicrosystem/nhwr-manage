<?php
$sql_person_parent_info = <<<SQL
SELECT 
	fld.name, fld.type,
	CASE 
		WHEN fld.type = 'string' then e.string_value
		WHEN fld.type = 'integer' then e.integer_value
		WHEN fld.type = 'text' then e.text_value
		WHEN fld.type = 'date' then e.date_value
	end value
FROM form_field ff
	INNER JOIN form f on ff.form = f.id
	INNER JOIN field fld on ff.field = fld.id
	LEFT JOIN record r on f.id = r.form and parent_form = 'person'
	LEFT JOIN last_entry e on ff.id = e.form_field and e.record = r.id
	LEFT JOIN field_sequence fs on ff.id = fs.form_field
WHERE f.name = 'person_parent_info'
	AND r.parent_id = 1
ORDER BY fs.sequence;
SQL;
/**
* 
*/
class i2ce_form
{
	
	// function __construct(argument)
	// {
	// 	# code...
	// }


	function loadFormData($form, $param){

	}
	private function fetch_data($form, $param){
		
	}

	function loadFormDatas($form, $param){

	}
}