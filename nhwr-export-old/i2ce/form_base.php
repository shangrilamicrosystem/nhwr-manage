<?php
// require_once "../DB.php";
/**
* 
*/
class form_base
{
	public $form = "";
	public $parent_form = "person";
	public $db = null;
	public $transpose = true;
	function __construct()
	{
		// code...
	}


	function loadFormData($parent, $param){
		$sql = $this->prepareSql($parent, $param);
		$records = $this->fetch_data($sql);
		if($this->transpose)
			$rec = $this->transpose_row($records);
		else
			$rec = $records[0];
		return $rec;

	}
	function prepareSql($parent, $param){

	}
	function fetch_data($sql){
		$db = new DB();
		$db->connect();
		$result = $db->conn->query($sql);
		$records = array();
		if($result->num_rows > 0){
			$cnt = 0;
			// output data of each row
		    while($row = $result->fetch_assoc()) {
		    	array_push($records, $row);
		    }
		}
		$db->close();
		return $records;
	}

	function transpose_row($records){
		$rec = array();
		foreach ($records as $row) {
			$rec[$row['name']] = $row['value'];
		}
		return $rec;

	}

	function loadFormDatas($param){

	}
}