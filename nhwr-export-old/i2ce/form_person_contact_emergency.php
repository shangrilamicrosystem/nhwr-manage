<?php
require_once "form_base.php";

class form_person_contact_emergency extends form_base{
	function __construct()
	{
		$this->form = "person_contact_emergency";
		$this->transpose = false;
	}

	function prepareSql($parent, $param){
		if(strlen($parent) == 0)
			return null;
		$sql = "
SELECT 
	c.firstname emergency_firstname, c.middlename emergency_middlename, c.surname emergency_surname,
	c.telephone emergency_telephone, c.mobile_phone emergency_mobile_phone, c.alt_telephone emergency_alt_telephone, c.email emergency_email
FROM hippo_person p 
	LEFT JOIN hippo_person_contact_emergency c on p.id = c.parent
WHERE p.id = '$parent'
";
		return $sql;
	}
}