<?php
error_reporting(E_ALL); ini_set('display_errors', 1);
require_once "export_base.php";
require_once "i2ce/form_person_parent_info.php";
require_once "i2ce/form_demographic.php";
require_once "i2ce/form_person_contact_personal.php";
require_once "i2ce/form_person_contact_emergency.php";
require_once "i2ce/form_person_employment_summary.php";

$sql = <<<SQL
SELECT 
	p.id, p.uuid, p.firstname, p.middle_name, p.surname, p.maiden_name, p.othername, p.othername_2, p.othername_3,
	c.name citizenship, c2.name citizenship_secondary,
	c_c.name residence_country, c_r.name residence_region, c_d.name residence_district, c_v.name residence_vdc_mun,
	p.ward_number, p.street_name, p.house_number
FROM hippo_person p
	LEFT JOIN hippo_country c on p.nationality = c.id
	LEFT JOIN hippo_country c2 on p.nationality_secondary = c2.id
	LEFT JOIN hippo_county c_v on p.residence = c_v.id
	LEFT JOIN hippo_district c_d on c_v.district = c_d.id or p.residence = c_d.id
	LEFT JOIN hippo_region c_r on c_d.region = c_r.id or p.residence = c_r.id
	LEFT JOIN hippo_country c_c on c_r.country = c_c.id or p.residence = c_c.id
SQL;

$db = new DB();
$db->connect();

function append_header(&$header, $record){
	foreach ($record as $key=>$value) {
		array_push($header, $key);
	}
}

//executing main sql related to person
$result = $db->conn->query($sql);
if($result->num_rows > 0){
	$cnt = 0;
	// $file = fopen("contacts.csv","w");
	$exporter = new ExportDataExcel('browser', 'persons.xls');
	$exporter->initialize(); // starts streaming data to web browser
	// output data of each row
    while($row = $result->fetch_assoc()) {
		$hdr = array("S.No.");
    	if($cnt ==0){
    		append_header($hdr, $row);
    	}
		//now adding row data related to the person (person detail & demographics)
		$data = array($cnt+1);
		foreach ($row as $value) {
			array_push($data, $value);
		}
		//### now addign parent info
		$frm = new form_person_parent_info();
		$record = $frm->loadFormData($row["id"], array());
		if($cnt == 0)
			append_header($hdr, $record);
		foreach ($record as $value) {
			array_push($data, $value);
		}

		//### now addign demographics info
		$frm = new form_demographic();
		$record = $frm->loadFormData($row["id"], array());
		if($cnt == 0)
			append_header($hdr, $record);
		foreach ($record as $value) {
			array_push($data, $value);
		}

		//### now adding employment summary
		$frm = new form_person_employment_summary();
		$record = $frm->loadFormData($row["id"], array());
		if($cnt == 0)
			append_header($hdr, $record);
		foreach ($record as $value) {
			array_push($data, $value);
		}


		//### now addind personal contact detail
		$frm = new form_person_contact_personal();
		$record = $frm->loadFormData($row["id"], array());
		if($cnt == 0)
			append_header($hdr, $record);
		foreach ($record as $value) {
			array_push($data, $value);
		}

		//### now addind emergency contact detail
		$frm = new form_person_contact_emergency();
		$record = $frm->loadFormData($row["id"], array());
		if($cnt == 0)
			append_header($hdr, $record);
		foreach ($record as $value) {
			array_push($data, $value);
		}

		if($cnt == 0){
			//now addign parent info header
			$exporter->addRow($hdr); 
		}
    	$cnt++;
		$exporter->addRow($data); 
    }
    $exporter->finalize();
    // echo "Record(s) exported successfully.";
}
else{
	echo "No records found.";
}
$db->close();