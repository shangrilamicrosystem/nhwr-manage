<?php
	$servername = "localhost";
	$username = "nhwr";
	$password = "nhwradmin";
	$db_name = "nhwr_with_hurdis_data_new";

	// Create connection
	$conn = new mysqli($servername, $username, $password, $db_name);

	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	}



/**
 * Convert a multi-dimensional, associative array to CSV data
 * @param  array $data the array of data
 * @return string       CSV text
 */
function str_putcsv($data) {
    # Generate CSV data from array
    $fh = fopen('php://temp', 'rw'); # don't create a file, attempt
                                     # to use memory instead

    # write out the headers
    fputcsv($fh, array_keys(current($data)));

    # write out the data
    foreach ( $data as $row ) {
            fputcsv($fh, $row);
    }
    rewind($fh);
    $csv = stream_get_contents($fh);
    fclose($fh);

    return $csv;
}
