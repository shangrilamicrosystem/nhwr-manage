<?php
    ini_set( 'error_reporting', E_ALL );
    require_once "../DB.php";
    
    //create temporary table & save data
    $sql = "SELECT 1 
            FROM information_schema.tables
            WHERE table_schema = '$db_name' 
                AND table_name = 'temp_person_degree'
            LIMIT 1";
    $result = $conn->query($sql);
    if ($result->num_rows < 1) {
        $conn->query("
            CREATE TABLE `temp_person_degree` (
              `person_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
              `degree_id` double(17,0) DEFAULT NULL,
              `classification_id` varchar(255) DEFAULT NULL,
              KEY `temp_person_degree_id_idx` (`person_id`) USING BTREE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8");
    }
    //populate the temporary table.
    $result = $conn->query("Truncate table temp_person_degree");

    $sql = "
insert into temp_person_degree(person_id, degree_id, classification_id)
select p.id person_id,  p.degree degree_id
	, min(REPLACE(mo.nhwr_code,'classification|','')) classification_id
from (
	select et.id 
		, floor(coalesce(max(et.edu_type_1), max(et.edu_type_2) , max(et.edu_type_3) , max(et.edu_type_4))) degree
	from (
		SELECT p.id, le.string_value, hd.edu_type  
			, case when hd.edu_type = 'edu_type|1' then replace(hd.id, 'degree|','') end edu_type_1
			, case when hd.edu_type = 'edu_type|2' then replace(hd.id, 'degree|','') end edu_type_2
			, case when hd.edu_type = 'edu_type|3' then replace(hd.id, 'degree|','') end edu_type_3
			, case when hd.edu_type = 'edu_type|4' then replace(hd.id, 'degree|','') end edu_type_4
		FROM record r
			INNER JOIN last_entry as le on le.record = r.id -- and CONCAT(r.parent_form ,'|',r.parent_id) = 'person|332437'
			INNER JOIN form_field ff on ff.id = le.form_field 
			INNER JOIN field f ON ff.field = f.id
			INNER JOIN hippo_person p on CONCAT(r.parent_form ,'|',r.parent_id) = p.id
			LEFT JOIN hippo_demographic d on p.id = d.parent
			LEFT JOIN hippo_degree hd on le.string_value = hd.id
		WHERE ff.form = 62
			AND f.`name` in ('degree')
		) et
	group by et.id
	) as p
	LEFT JOIN map_degree_occupation mdc on mdc.nhwr_degree_code = CONCAT('degree|',p.degree)
	left join map_occupation mo on mo.who_code = mdc.who_occupation_code
group by p.id,p.degree";
    $result = $conn->query($sql);
    
    //reset temp_zebra_position table
    require "temp_zebra_position_list_table.php";

    $data_sql = 
    "SELECT
    	-- DEMOGRAPHICS
    	case when d.gender = 'gender|F' THEN 'F' ELSE 'M' end gender
    	, TIMESTAMPDIFF(YEAR, d.birth_date, NOW()) age, extract(year from d.birth_date) dob_year_ad	
    	, replace(p.nationality,'country|','') nationality_id
    	, REPLACE(co.id ,'county|', '') residence_county_id, REPLACE(di.id ,'district|', '') residence_district_id 
    	, pr.province_no residence_province, REPLACE(re.id ,'region|', '') residence_region_id, REPLACE(c.id ,'country|', '') residence_country_id
    	
    	,  REPLACE(d.marital_status ,'marital_status|', '') marital_status_id
    	-- WORKING
    	 , REPLACE(f.id,'facility|','') facility_id, replace(f.facility_type,'facility_type|','') facility_type_id
    	 , replace(j.id, 'job|','') job_id, COALESCE(replace(j.classification,'classification|',''), nw.classification_id) job_classification_id
    	 , date(pl.`current+start_date`) join_date_ad
    	, REPLACE(fco.id ,'county|', '') facility_county_id, REPLACE(fdi.id ,'district|', '') facility_district_id 
    	, REPLACE(fre.id ,'region|', '') facility_region_id, fpr.province_no facility_province
    	-- retirement
    	, extract(year from DATE_ADD(d.birth_date, interval 58 year)) retirement_year
    FROM hippo_person p 
    	left JOIN temp_zebra_position_list pl on pl.`current+parent` = p.id
    	-- demographics
    	LEFT JOIN hippo_demographic d on pl.`current+parent` = d.parent 
    	LEFT JOIN hippo_county co on p.residence = co.id
    	LEFT JOIN hippo_district di on p.residence = di.id OR co.district = di.id
    	LEFT JOIN hippo_region re on p.residence = re.id OR di.region = re.id
    	LEFT JOIN map_district_province pr on di.id = pr.nhwr_district_code
    	LEFT JOIN hippo_country c on p.residence = c.id OR re.country = c.id
    	-- WORKING DETAIL
    	left join temp_person_degree nw on p.id = nw.person_id
    	LEFT JOIN hippo_facility f on pl.`primary_form+facility` = f.id
    	LEFT JOIN hippo_job j on pl.`primary_form+job`= j.id
    	LEFT JOIN hippo_county fco on f.location = fco.id
    	LEFT JOIN hippo_district fdi on f.location = fdi.id OR fco.district = fdi.id
    	LEFT JOIN hippo_region fre on f.location = fre.id OR fdi.region = fre.id
    	LEFT JOIN map_district_province fpr on fdi.id = fpr.nhwr_district_code
    LIMIT limit_value offset offset_value;
	";
    
    $sql_count = "select count(1) as count from `hippo_person`;";
    $result = $conn->query($sql_count);
    $row = $result->fetch_assoc();
    $count = $row['count'];
    //put any number for quick test
    //$count = 1500;
    $limit = 5000; 
    $offset = 0; 
    $data_for_csv = array();
    $dest_file = getcwd(). "/data.csv";
    $csv_file = getcwd(). "/data_temp.csv";

    if(file_exists($csv_file))
        unlink($csv_file);
    
    $is_header_written = false;
    try
    {
        for($i = 0; $i <= $count; $i+=$limit){
            $offset = $i;
            $value = ["limit_value", "offset_value"];
            $replace   = [$limit, $offset];
            $sql = str_replace($value, $replace, $data_sql);
            $result = $conn->query($sql);
            if (!$result) {
                printf("Errormessage: %s\n", $conn->error);
                die();
            }
            
            if ($result->num_rows > 0) {
                try{
                    $handle = fopen($csv_file, "a");
                    // output data of each row
                    while($row = $result->fetch_assoc()) {
                        foreach ($row as $key => $value) {
                            $d[$key] = $value;
                        }
                        // # write out the headers
                        if($is_header_written == false){
                            $is_header_written = true;
                            fputcsv($handle, array_keys($d));
                        }
                            
                        // $data_for_csv[] = $d;
                        fputcsv($handle, $d);
                    }
                    fclose($handle);
                }
                catch(Exception $e){
                    fclose($handle);
                }
            }
            echo $limit. " >> ". $offset. "\n";
        }
    }
    catch(Exception $e){

    }
    
    if(file_exists($csv_file)){
        if(file_exists($dest_file)){
            unlink($dest_file);           
        }
        rename($csv_file, $dest_file);
    }
    
	echo "done";
	$conn->close();
?>