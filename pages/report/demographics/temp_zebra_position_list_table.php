<?php
    ini_set( 'error_reporting', E_ALL );
    require_once "../DB.php";
    
    //create temporary table & save data
    $sql = "SELECT 1 
            FROM information_schema.tables
            WHERE table_schema = '$db_name' 
                AND table_name = 'temp_zebra_position_list'
            LIMIT 1";
    $result = $conn->query($sql);
    if ($result->num_rows < 1) {
        $conn->query("
            CREATE TABLE `temp_zebra_position_list` (
              `last_modified` datetime DEFAULT NULL,
              `primary_form+id` varchar(255) COLLATE utf8_bin DEFAULT '',
              `primary_form+parent` varchar(255) COLLATE utf8_bin DEFAULT NULL,
              `primary_form+department` varchar(255) COLLATE utf8_bin DEFAULT NULL,
              `primary_form+facility` varchar(255) COLLATE utf8_bin DEFAULT NULL,
              `primary_form+job` varchar(255) COLLATE utf8_bin DEFAULT NULL,
              `primary_form+posted_date` datetime DEFAULT NULL,
              `primary_form+code` varchar(255) COLLATE utf8_bin DEFAULT NULL,
              `primary_form+status` varchar(255) COLLATE utf8_bin DEFAULT NULL,
              `primary_form+title` varchar(255) COLLATE utf8_bin DEFAULT NULL,
              `current+id` varchar(255) COLLATE utf8_bin DEFAULT '',
              `current+parent` varchar(255) COLLATE utf8_bin DEFAULT NULL,
              `current+start_date` datetime DEFAULT NULL,
              `current+end_date` datetime DEFAULT NULL,
              `department+id` varchar(255) COLLATE utf8_bin DEFAULT '',
              `department+parent` varchar(255) COLLATE utf8_bin DEFAULT NULL,
              `department+name` text COLLATE utf8_bin,
              `facility+id` varchar(255) COLLATE utf8_bin DEFAULT '',
              `facility+parent` varchar(255) COLLATE utf8_bin DEFAULT NULL,
              `facility+name` text COLLATE utf8_bin,
              `job+id` varchar(255) COLLATE utf8_bin DEFAULT '',
              `job+parent` varchar(255) COLLATE utf8_bin DEFAULT NULL,
              `job+classification` varchar(255) COLLATE utf8_bin DEFAULT NULL,
              `job+cadre` varchar(255) COLLATE utf8_bin DEFAULT NULL,
              `job+title` varchar(255) COLLATE utf8_bin DEFAULT NULL,
              `last+id` varchar(255) COLLATE utf8_bin DEFAULT '',
              `last+parent` varchar(255) COLLATE utf8_bin DEFAULT NULL,
              `last+end_date` datetime DEFAULT NULL,
              `classification+id` varchar(255) COLLATE utf8_bin DEFAULT '',
              `classification+parent` varchar(255) COLLATE utf8_bin DEFAULT NULL,
              `classification+name` text COLLATE utf8_bin,
              `cadre+id` varchar(255) COLLATE utf8_bin DEFAULT '',
              `cadre+parent` varchar(255) COLLATE utf8_bin DEFAULT NULL,
              `cadre+name` text COLLATE utf8_bin,
              `last_md5` binary(16) DEFAULT NULL,
              `md5` binary(16) DEFAULT NULL,
              `+days_open` int(11) DEFAULT NULL,
              UNIQUE KEY `last_md5` (`last_md5`),
              UNIQUE KEY `md5` (`md5`),
              KEY `primary_form+id` (`primary_form+id`),
              KEY `last_modified` (`last_modified`),
              KEY `primary_form+department` (`primary_form+department`),
              KEY `primary_form+facility` (`primary_form+facility`),
              KEY `primary_form+job` (`primary_form+job`),
              KEY `primary_form+posted_date` (`primary_form+posted_date`),
              KEY `primary_form+code` (`primary_form+code`),
              KEY `primary_form+status` (`primary_form+status`),
              KEY `primary_form+title` (`primary_form+title`),
              KEY `current+start_date` (`current+start_date`),
              KEY `current+end_date` (`current+end_date`),
              KEY `job+classification` (`job+classification`),
              KEY `job+cadre` (`job+cadre`),
              KEY `job+title` (`job+title`),
              KEY `last+end_date` (`last+end_date`),
              KEY `current+parent` (`current+parent`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

        ");
    }
    
    
    //create temporary table & save data
    $sql = "SELECT 1 
            FROM information_schema.tables
            WHERE table_schema = '$db_name' 
                AND table_name = 'zebra_position_list'
            LIMIT 1";
    $result = $conn->query($sql);
    if ($result->num_rows == 1) {
        
        //populate the temporary table.
        $result = $conn->query("Truncate table temp_zebra_position_list");
        
        $sql = "select * from zebra_position_list";
        $result = $conn->query($sql);
       
         while($row = $result->fetch_assoc()) {
            $sql = "insert into temp_zebra_position_list ";
            $column =  array_keys( $row );
            $col_data = array();
            foreach( $column as $col) {
              $col_data[] = "`".$col. "`";
            }
            $sql .= "( ". implode(",", $col_data). " ) Values(";

            $values = array();
            foreach($row as $key=>$val){
              if( $val == "0000-00-00 00:00:00" or $val == '') {
                $values[] = "NULL";
              }
              else{
                $values[] = "'". $val . "'";
              }
            }

            $sql .= implode(",", $values ) . " )";
            $conn->query($sql);
         }
    }
    
    echo "done insert to temp_zebra_position_list";
    
    