<?php
	require "../DB.php";
	/*
	select SUBSTRING_INDEX(id,'|',-1)  AS id, name from `hippo_district`;

	select SUBSTRING_INDEX(id,'|',-1) AS id, name from `hippo_region`;

	select SUBSTRING_INDEX(id,'|',-1)  AS id, name from `hippo_country`;

	select SUBSTRING_INDEX(id,'|',-1)  AS id, name from `hippo_county`;
	*/

	$district = "select SUBSTRING_INDEX(id,'|',-1)  AS id, name from `hippo_district` limit 30;";

	$region = "select SUBSTRING_INDEX(id,'|',-1) AS id, name from `hippo_region`;";


	$country = "select SUBSTRING_INDEX(id,'|',-1)  AS id, name from `hippo_country`;";

	$county = "select SUBSTRING_INDEX(id,'|',-1)  AS id, name from `hippo_county`;";

	$facilty = "select SUBSTRING_INDEX(id,'|',-1)  AS id, name from `hippo_facility`;";
	$facilty_type = "select SUBSTRING_INDEX(id,'|',-1)  AS id, name from `hippo_facility_type`;";

	$who_occupation = "select code, name from who_occupation;";

	$result = $conn->query($district);
	$district = array();
	if ($result->num_rows > 0) {
	    // output data of each row
	    while($row = $result->fetch_assoc()) {
	        // echo "id: " . $row["id"]. " - Name: " . $row["name"];
	        $district[$row['id']] = $row['name'];
	    }
	}

	//region
	$result = $conn->query($region);
	$region = array();
	if ($result->num_rows > 0) {
	    // output data of each row
	    while($row = $result->fetch_assoc()) {
	        // echo "id: " . $row["id"]. " - Name: " . $row["name"];
	        $region[$row['id']] = $row['name'];
	    }
	}

	//country
	$result = $conn->query($country);
	$country = array();
	if ($result->num_rows > 0) {
	    // output data of each row
	    while($row = $result->fetch_assoc()) {
	        // echo "id: " . $row["id"]. " - Name: " . $row["name"];
	        $country[$row['id']] = $row['name'];
	    }
	}

	//county
	$result = $conn->query($county);
	$county = array();
	if ($result->num_rows > 0) {
	    // output data of each row
	    while($row = $result->fetch_assoc()) {
	        // echo "id: " . $row["id"]. " - Name: " . $row["name"];
	        $county[$row['id']] = $row['name'];
	    }
	}

	
	//county
	$result = $conn->query($who_occupation);
	$who_occupation = array();
	if ($result->num_rows > 0) {
	    // output data of each row
	    while($row = $result->fetch_assoc()) {
	        // echo "id: " . $row["id"]. " - Name: " . $row["name"];
	        $who_occupation[$row['code']] = $row['name'];
	    }
	}

	//facilty
	$result = $conn->query($facilty);
	$facility = array();
	if ($result->num_rows > 0) {
	    // output data of each row
	    while($row = $result->fetch_assoc()) {
	        // echo "id: " . $row["id"]. " - Name: " . $row["name"];
	        $facility[$row['id']] = $row['name'];
	    }
	}
	$master['facility'] = $facility; 

	//facilty_type
	$result = $conn->query($facilty_type);
	$facility_type = array();
	if ($result->num_rows > 0) {
	    // output data of each row
	    while($row = $result->fetch_assoc()) {
	        // echo "id: " . $row["id"]. " - Name: " . $row["name"];
	        $facility_type[$row['id']] = $row['name'];
	    }
	}
	$master['facility_type'] = $facility_type; 

	$master['district'] = $district;
	$master['region'] = $region;
	$master['country'] = $country;
	$master['county'] = $county;
	$master['who_occupation'] = $who_occupation;


	//job
	$job = "select SUBSTRING_INDEX(id,'|',-1)  AS id, title from `hippo_job`;";
	$result = $conn->query($job);
	$data = array();
	if ($result->num_rows > 0) {
	    // output data of each row
	    while($row = $result->fetch_assoc()) {
	        // echo "id: " . $row["id"]. " - Name: " . $row["name"];
	        $data[$row['id']] = $row['title'];
	    }
	}
	$master['job'] = $data; 

	//job classification
	$classification = "select SUBSTRING_INDEX(id,'|',-1)  AS id, name from `hippo_classification`;";
	$result = $conn->query($classification);
	$data = array();
	if ($result->num_rows > 0) {
	    // output data of each row
	    while($row = $result->fetch_assoc()) {
	        // echo "id: " . $row["id"]. " - Name: " . $row["name"];
	        $data[$row['id']] = $row['name'];
	    }
	}
	$master['classification'] = $data; 
	
	
	$province = array(
	        1 => 'Province No. 1',
	        2 =>'Province No. 2',
	        3 =>'Province No. 3',
	        4 =>'Province No. 4',
	        5 =>'Province No. 5',
	        6 =>'Karnali Pradesh',
	        7 =>'Province No. 7'
	    );
	$master['province'] = $province;     
    
    $t = (60*10);
    $ts = gmdate("D, d M Y H:i:s", time() + $t) . " GMT";
    header('Content-Type: application/json');
    header("Expires: $ts");
    header("Pragma: cache");
    header("Cache-Control: max-age=$t");

	echo json_encode($master);
	
	$conn->close();



?>