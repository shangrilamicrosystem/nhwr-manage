window.pivotTableWorking = (function (params) {
    var getWorkingDerivedMapping = function () {
        var opt = {
            "Nationality": { master: "country", "data_field": "nationality_id" },
            // "Occupation" : {master: "who_occupation", "data_field": 'who_job_code'},
            "Residence Mun/VDc": {master:"county", data_field: 'residence_county_id'},
            "Residence District" : { master: "district", data_field: "residence_district_id"},
            "Residence Region" : { master: "region", data_field: "residence_region_id"},
            "Residence Province " : { master: 'province', data_field: "residence_province" },
            
            "Facility" : {master : "facility", data_field : 'facility_id'},
            "Facility Type": {master: 'facility_type', data_field: "facility_type_id"},
            "Facility Mun/VDc" : {master: "county", data_field: "facility_county_id"},
            "Facility District" : {master: "district", data_field: "facility_district_id"},
            "Facility Region" : {master: "region", data_field: "facility_region_id"},
            "Facility Province " : { master: 'province', data_field: "facility_province" },
            
            "Job": { master: "job", data_field: "job_id"},
            "Job Classification": { master: "classification", data_field: "job_classification_id"}
            
        };
        return opt;
    };
    var getWorkingLabels = function () {
        var opt = {
            "age" : "Age",
            "join_date_ad": "Join Date",
            "retirement_year" : "Retirement Year"
        };
        return opt;
    };
    var getDerivedAttributes = function(){
        var opt = {
            "Gender" : function(record){
                return record.gender == 'F' ? "महिला" : "पुरुष";    
            },
            "Marital Status" : function(record){
                return record.marital_status_id == '1' ? "Married" : '2' ? "Single": '3' ? "Widowed" : "Divorced";    
            }
        };
        return opt;
    };
    var loadAndRender = function (params) {
        params.hiddenAttributes = params.hiddenAttributes || [];
        params.hiddenAttributes.push("Gender");
        params.hiddenAttributes.push("country_id");

        params.hiddenAttributes.push("residence_country_id");
        params.hiddenAttributes.push("marital_status_id");
        params.hiddenAttributes.push("dob_year_ad");

        params.derivedAttributes = params.derivedAttributes || {};
        $.each(getDerivedAttributes(), function (key, item) {
            params.derivedAttributes[key] = item;
        });
        // derivedMapping
        params.derivedMapping = params.derivedMapping || {};
        $.each(getWorkingDerivedMapping(), function (key,item) {
            params.derivedMapping[key] = item;
        });
        // rows & cols
        params.rows = params.rows || [];
        params.cols = params.cols || [];
        // params.rows.push("युनिटको प्रकार");
        if(params.rows.length == 0){
            params.rows.push("वतन क्षेत्र");
            params.cols.push("पद");
        }

        params.labels = getWorkingLabels();
        pivotTableHelper.loadAndRender(params);
    };
    return {
        loadAndRender: loadAndRender
    }

})();