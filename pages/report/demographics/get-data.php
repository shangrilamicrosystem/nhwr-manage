<?php

    $file = getcwd(). "/data.csv";
    $mtime = filemtime($file);
    $gmdate_mod = gmdate('D, d M Y H:i:s', $mtime) . ' GMT';
    
    $t = (60*10);
    $ts = gmdate("D, d M Y H:i:s", time() + $t) . " GMT";
    header('Content-Type: text/csv');
    header("Expires: $ts");
    header("Pragma: cache");
    header("Cache-Control: max-age=$t");

    $content = file_get_contents($file);
    
    // header("Cache-Control: max-age=18000"); // (2592000 )30days (60sec * 60min * 24hours * 30days)
    echo $content;